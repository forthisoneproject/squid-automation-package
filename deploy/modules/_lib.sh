#!/bin/bash
# Common functions library

## List of available functions:
#
# * exit_if_error    - simply exit, if supplied exit status is not zero
# * get_os_name      - prints current OS name to stdin
# * step_crit        -
#

# Call this function to break execution of the script when it's not run as root
# syntax:
# require_root
#
function require_root {
  if [ $(id -u) -ne "0" ]; then
    echo "ERROR: This script should be executed by root. Aborting."
    exit 10
  fi
}

# Call this function to break execution of the script
# syntax:
# exit_if_error $?
#
function exit_if_error {
  status=$1
  message="$2"
  if [ "$status" -ne 0 ]; then
    echo "$message" FAILED
    exit $status
  fi
}

# Get current OS name
# syntax:
# OS_NAME=`get_os_name`
#
function get_os_name {
   sed -n 's/"//g; s/^ID=// p' /etc/*release
}

# Generic wrapper for calling external commands and scripts
# Writes summary to STDIN and redirects all data from command into log file
# use "step_norm" and "step_crit" functions to simplify calls.
# syntax:
# step_generic [0|1] /path/to/log "Message for console" COMMAND PARAMETERS
#
function step_generic {
   iscritical="$1"
   log_file="$2"
   message="$3"
   shift 3

   touch $log_file
   if [ ! -w "$log_file" ]; then
      echo -e "\n\033[1;31mERROR\033[0m: Can't access log file $log_file\n"
      exit 1
   fi

   # Summary for user
   printf "%60s ... " "$message"

   # Details for log
   echo -e "\n\n#### Executing step: $message\n#### Command line: $@\n" >> $log_file

   $@  &>> $log_file
   status=$?

   if [ $status -eq 0 ]; then
      echo -e "\033[1;32mOK\033[0m"
   else
      echo -e "\033[1;31mFAILED\033[0m"
      echo -e "See log file for details: $log_file"
      if [ "$iscritical" -ne 0 ]; then exit $status; fi
   fi

   return $status
}

# Wrapper for "critical" configuration step (exit if failed)
# syntax:
# step_crit "Description" COMMAND PARAMETERS
#
function step_crit {
   step_generic 1 $STEP_LOG "$@"
   return $?
}

# Wrapper for "non-critical" configuration step (continue even if failed)
# syntax:
# step_norm "Description" COMMAND PARAMETERS
#
function step_norm {
   step_generic 0 $STEP_LOG "$@"
   return $?
}

# Backup current config
# syntax:
# backup_config /path/to/file $LIST_OF_FULL_PATHS
function backup_config {
   curr_date=`date "+%Y%m%d-%H%M"`
   bak_file="$1.$curr_date.tar"
   bak_list="$2"

   echo "Creating backup of current configuration in file: $bak_file"

   # Create backup
   echo "$bak_list" | sed -r "s/^\///" | tar --create -C / --file $bak_file --files-from -

   return $?
}
