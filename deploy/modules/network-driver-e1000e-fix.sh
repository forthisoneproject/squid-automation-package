#!/bin/bash
# NIC driver issues fix
#

# Configurable parameters
global_conf_dir="/srv/aps-proxy"

elrepo_key_url="https://www.elrepo.org/RPM-GPG-KEY-elrepo.org"
elrepo_rpm_url="http://www.elrepo.org/elrepo-release-6-6.el6.elrepo.noarch.rpm"
driver_package_name="kmod-e1000e"

grub_conf_path="/boot/grub/grub.conf"

# Automatic parameters

prog_name=`basename $0`
prog_dir=$(readlink -f `dirname $0`)

lib_file="$prog_dir/_lib.sh"
if [ ! -e "$lib_file" ]; then
  echo "ERROR: can't load functions library $lib_file"
  exit 1
fi
. $lib_file

require_root

backup_list="$grub_conf_path"
main_interface=`cat $global_conf_dir/interface`

# Check current if current system needs this fix
function check_e1000e {
  echo "Current system Ethernet controllers:"
  lspci | grep Ethernet

  echo -e "\n\nCurrent system kernel modules:"
  lsmod

  echo -e "\n\nPrimary interface info:"
  ethtool -i $main_interface

  curr_nic_driver=`ethtool -i $main_interface | sed -n 's/^driver: // p'`
  exit_if_error $? "Detect current NIC driver"

  if [ "$curr_nic_driver" != "e1000e" ]; then
    echo "Current NIC driver is $curr_nic_driver, not e1000e. This fix is not needed."
    return 1
  fi

  return 0
}

# Install and fix NIC driver
function install_nic_driver {
  sudo rpm --import $elrepo_key_url && \
  rpm -Uvh $elrepo_rpm_url && \
  yum install -y $driver_package_name
  exit_if_error $? "elrepo and e1000e installation"
}

# Disable pcie_aspm during boot
function disable_pcie_aspm {
  sed -r 's/ pcie_aspm=[a-z]+//; s/(kernel .+vmlinuz.+$)/\1 pcie_aspm=off/' $grub_conf_path
  return $?
}

# Perform actions
check_e1000e
[ $? -eq 0 ] || exit 0

backup_config $global_conf_dir/backup/network-driver-e1000e-fix "$backup_list"
exit_if_error $? "Creation of backup"

install_nic_driver
disable_pcie_aspm

service network restart
