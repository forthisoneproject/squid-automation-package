#!/bin/bash
# Install or update Statistics configuration
#

# Configurable parameters
global_conf_dir="/srv/aps-proxy"
stat_dir="/var/spool/squid"
stat_owner="squid"
stat_group="squid"
action=""

# Automatic parameters
prog_name=`basename $0`
prog_dir=$(readlink -f `dirname $0`)


lib_file="$prog_dir/_lib.sh"
if [ ! -e "$lib_file" ]; then
  echo "ERROR: can't load functions library $lib_file"
  exit 1
fi
. $lib_file

require_root

function print_help {
   echo -e "\nExample:\n\n\t$prog_name --action install\n
\tOptions:
\t\t--action - Action type {install|update}\n\n"
}

function process_opts {
   shortopts="i:t:"
   longopts="action:"

   if [ -z "$1" ]; then print_help ; exit 1; fi

   args=$(getopt -o "$shortopts" -l "$longopts" -n $prog_name -- "$@")

   if [ $? -ne 0 ]; then print_help ; exit 1; fi

   eval set -- "$args"

   while true; do
      case "$1" in
         --action)
            action="$2"
            shift 2
            ;;
         --)
            shift
            if [ -z "$action" ]; then
               echo "ERROR: action is not specified."
               exit 1
            fi
            break
            ;;
         *)
            echo "ERROR: Unknown option: $1. Please check your command syntax."
            exit 1
            ;;
      esac
   done
}

process_opts "$@"


function action_install {
  echo "Creating spool directories"
  install_cmd="install --verbose --mode=755 --owner=$stat_owner --group=$stat_group"
  $install_cmd --directory $stat_dir/stat-upload    &&
  $install_cmd --directory $stat_dir/stat-archive
  [ $? -eq 0 ] || return 1

  return 0
}

function action_update {
  echo "Updating stat settings"
  install --verbose --mode=600 --owner=$stat_owner \
    $prog_dir/../system-config/ssh-auth-stat \
    $global_conf_dir/ssh-auth-stat

  return $?
}

case "$action" in
   install)
      action_install &&
      action_update
      ;;
   update)
      action_update
      ;;
   *)
      echo "ERROR: Unknown action: $action"
      exit 1
      ;;
esac
