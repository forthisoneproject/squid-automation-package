#!/bin/bash

progname=$0
OS_VERSION=''

do_help()
{
  printf "
  Example:

    $progname --os_version centos

  Options:

    -O,--os_version - operation system type {debian|ubuntu|centos}

    -h,--help - print help information

"
}

shortopts="hO:"
longopts="help,os_version:"

args=$(getopt -o "$shortopts" -l "$longopts" -n $progname -- "$@")

if [[ $? -ne 0 || -z $@ ]]; then
  do_help
  exit 1
fi

eval set -- "$args"

while true; do
  case "$1" in
    -h|--help)
      do_help
      exit 0
    ;;
    -O|--os_version)
      OS_VERSION="$2"
      shift 2
    ;;
    --)
      shift
      if [ -z "$OS_VERSION" ]; then
        echo "Not enough options was set. Please check your call"
        exit 1
      fi
      break
    ;;
    *)
      echo "Script cannot parse your arguments correctly. Sorry"
      exit 1
    ;;
  esac
done

function install_centos {
  if rpm -q firewalld >/dev/null 2>&1; then
    systemctl disable firewalld
    systemctl stop firewalld
  fi
  yum -y install iptables-services &&
  systemctl enable iptables &&
  tee /etc/sysconfig/iptables <<RULES | iptables-restore &&
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
COMMIT
RULES
  iptables -L &&
  systemctl restart iptables

  return $?
}

function install_ubuntu {
   apt-get -y install iptables &&
   mkdir /etc/iptables > /dev/null 2>&1 &&
   cat > /etc/iptables/rules.v4 <<RULES
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
COMMIT
RULES
   cfg_file=/etc/rc.local
   if [ -z "$(cat $cfg_file | grep 'iptables-restore < /etc/iptables/rules.v4')" ]; then
     sed -ri 's/^\s*exit 0/iptables-restore < \/etc\/iptables\/rules.v4\n&/' "$CFG_FILE"
   fi
   iptables-restore < /etc/iptables/rules.v4

   return $?
}

case "$OS_VERSION" in
   centos)
      install_centos
   ;;
   debian|ubuntu)
      install_ubuntu
   ;;
   *)
      echo "ERROR: Unknown OS version $OS_VERSION"
      exit 1
   ;;
esac

exit $?
