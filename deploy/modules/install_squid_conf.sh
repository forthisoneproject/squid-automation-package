#!/bin/bash
# Install or update Squid configuration
#

# Configurable parameters
global_conf_dir="/srv/aps-proxy"
squid_conf_dir="/etc/squid"
dynamic_conf_dir="$global_conf_dir/config.dynamic"

squid_main_conf="$squid_conf_dir/squid.conf"
squid_local_conf="$squid_conf_dir/local.conf"
squid_ipranges_dir="$squid_conf_dir/ip-ranges.dynamic"
squid_init_script="/etc/init.d/squid"

# Automatic parameters
prog_name=`basename $0`
prog_dir=$(readlink -f `dirname $0`)

scripts_src_dir="$prog_dir/../scripts-squid"

lib_file="$prog_dir/_lib.sh"
if [ ! -e "$lib_file" ]; then
  echo "ERROR: can't load functions library $lib_file"
  exit 1
fi
. $lib_file

require_root

action=""
backup_list="$squid_conf_dir"

function print_help {
   echo -e "\nExample:\n\n\t$prog_name --action install\n
\tOptions:
\t\t--action - Action type {install|update}\n\n"
}

function process_opts {
   shortopts="i:t:"
   longopts="action:"

   args=$(getopt -o "$shortopts" -l "$longopts" -n $prog_name -- "$@")

   if [[ $? -ne 0 ]]; then
      print_help
      exit 1
   fi

   eval set -- "$args"

   while true; do
      case "$1" in
         --action)
            action="$2"
            shift 2
            ;;
         --)
            shift
            if [ -z "$action" ]; then
               echo "ERROR: action is not specified."
               exit 1
            fi
            break
            ;;
         *)
            echo "ERROR: Unknown option: $1. Please check your command syntax."
            exit 1
            ;;
      esac
   done
}

process_opts "$@"

function fix_privileges {
  # Fix file permissions
  chown -R root:squid $squid_conf_dir &&
  chmod -R a+rX $squid_conf_dir
  exitstatus=$(($exitstatus+$?))

  return $?
}


function action_install {
  rm $squid_conf_dir/*.default

  # Creating local configuration file
  host_name=`hostname`
  cat > $squid_local_conf <<SQUID_CONF
# Host name
visible_hostname $host_name
SQUID_CONF

  [ $? -ne 0 ] && return 1


  # Create new directories and files
  mkdir -p $squid_conf_dir/scripts          &&
  mkdir -p $squid_ipranges_dir              &&
  touch $squid_ipranges_dir/ip-ranges.conf  &&

  action_update

  return $?
}

function action_update {
  # Create basic config
  install --verbose --mode=644 $prog_dir/../squid.conf $squid_conf_dir &&

  cat > $squid_conf_dir/our_networks.txt <<TXT_FILE &&
# List of allowed clients/servers
127.0.0.1
TXT_FILE

  # Scripts
  install --verbose --mode=755 -t $squid_conf_dir/scripts $scripts_src_dir/*.sh &&

  cat > $squid_conf_dir/authentication.conf  <<EOF &&
# External authentication for userIP-myIP pairs
external_acl_type userip-myip-auth ttl=3600 negative_ttl=60 children-max=30 children-startup=5 grace=0 %SRC %MYADDR $squid_conf_dir/scripts/user-auth-helper.sh $dynamic_conf_dir/auth.userip-proxyip.squid
acl userip-myip-authenticated external userip-myip-auth
http_access allow userip-myip-authenticated

# External authentication for users

auth_param basic children 5
auth_param basic program $squid_conf_dir/scripts/user-auth-helper.sh $dynamic_conf_dir/auth.user-password.squid
auth_param basic realm APS proxies authentication
auth_param basic credentialsttl 1 hour
acl user-authenticated  proxy_auth REQUIRED
#acl user-fullaccess     proxy_auth aps-admin

# External authentication for user-myIP pairs
external_acl_type user-myip-auth ttl=3600 negative_ttl=60 children-max=20 children-startup=2 grace=0 %LOGIN %MYADDR $squid_conf_dir/scripts/user-auth-helper.sh $dynamic_conf_dir/auth.user-proxyip.squid
acl user-myip-authenticated external user-myip-auth

## Port-specific password auth
# acl myport_userauth myport 8080
# http_access deny  myport_userauth !user-authenticated       all
# http_access allow myport_userauth  user-fullaccess
# http_access allow myport_userauth  user-myip-authenticated

# http_access allow user-fullaccess
http_access deny !user-authenticated       all
http_access allow user-myip-authenticated

EOF

  # Log rotation settings
  install --verbose --mode=644 $prog_dir/../system-config/logrotate.squid /etc/logrotate.d/squid &&

  fix_privileges

  return $?
}

case "$action" in
   install)
      action_install
      ;;
   update)
      backup_config $global_conf_dir/backup/squid_config_update "$backup_list" &&
      action_update
      ;;
   *)
      echo "ERROR: Unknown action: $action"
      exit 1
      ;;
esac
