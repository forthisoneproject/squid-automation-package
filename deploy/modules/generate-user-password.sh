#!/bin/bash
# Generate a random password for specified user

user_name="$1"
if [ -z "$user_name" ]; then
  echo "ERROR: user name is not specified"
  exit 1
fi

tmp_file=`mktemp`
dd if=/dev/urandom of=$tmp_file bs=1 count=20 2> /dev/null

rnd_password=`cat $tmp_file | base64`
rm $tmp_file

echo "Setting new password for user $user_name"
echo "$user_name:$rnd_password" | chpasswd

if [ $? -eq 0 ]; then
  echo "New password is set: $rnd_password"
else
  echo "Error while setting new password for user $user_name"
  exit 1
fi
