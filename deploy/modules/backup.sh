#!/bin/bash
# Create a backup of current configuration

# Configurable parameters
global_conf_dir="/srv/aps-proxy"

bak_files_list="/etc
$global_conf_dir"

# Backup current config
# syntax:
# backup_config /path/to/file $LIST_OF_FULL_PATHS
function backup_config {
   curr_date=`date "+%Y%m%d-%H%M"`
   bak_file="$1.$curr_date.tgz"
   bak_list="$2"

   echo "Creating backup of current configuration in file: $bak_file"

   # Create backup
   echo "$bak_list" | sed -r "s/^\///" | tar --create --exclude="srv/aps-proxy/backup" -z -C / --file $bak_file --files-from -

   return $?
}

backup_config "$global_conf_dir/backup/full-config-backup"  "$bak_files_list"
