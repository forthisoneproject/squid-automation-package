#!/bin/bash

# Check if this script has root credentials
if [ $(id -u) -ne "0" ]; then  echo "This script should be executed by root. Aborting."; exit 1; fi

### System user add
sys_add_user() {
	USER_NAME=$1
	USER_HOME="/home/$USER_NAME"
	USER_SUDOERS="/etc/sudoers.d/$USER_NAME"
	SSH_DIR="$USER_HOME/.ssh"
	SSH_KEYS="$SSH_DIR/authorized_keys"

	useradd -m $USER_NAME && \
	mkdir $SSH_DIR && \
	chmod 700 $SSH_DIR && \
	chown $USER_NAME $SSH_DIR && \
	touch $SSH_KEYS && \
	chown $USER_NAME $SSH_KEYS && \
	chmod 600 $SSH_KEYS

	cat > $SSH_KEYS <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAoueKojeEgAjKEqw4iNEZ5K8CjtYa8XvZvqNyONJxNoimm3nkZQF/aDPmIJP2xqb4P+JbDG0sBA5LnE0v2wTwOxpQtFy4L0NKKwD0keaMj160NAS7XX9aNunh/NTQwHwNvjAcqhR4UanjSxkoqqe8KXtNlhqzEoIv0vUJiGwKynB9Ptd9uQ21k7OFD8aq7mwY7wYXJoyNpxRox0f04uf1Twa2JpkjnEf6KGiUVFZhlX1XG1djwGbDOaSh0bvoVb5VqM7c7c6UmnOyMNc/LoG/8vTdUIrux9CJY6zJ4Xu7jft1ODWypKhcjU9MHP+buFyNrC3vPQdfhlDZVmkjvh+3dw== ag.softevol
EOF

	echo "$USER_NAME ALL=(ALL) NOPASSWD:ALL" > $USER_SUDOERS
}

sys_add_user aps-admin
