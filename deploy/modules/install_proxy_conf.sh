#!/bin/bash
# Install or update APS proxy global configuration
#

# Configurable parameters
global_conf_dir="/srv/aps-proxy"
squid_conf_dir="/etc/squid"
admin_user="aps-admin"

# Automatic parameters
prog_name=`basename $0`
prog_dir=$(readlink -f `dirname $0`)

scripts_src_dir="$prog_dir/../scripts-global"

lib_file="$prog_dir/_lib.sh"
if [ ! -e "$lib_file" ]; then
  echo "ERROR: can't load functions library $lib_file"
  exit 1
fi
. $lib_file

require_root

action=""

function print_help {
   echo -e "\nExample:\n\n\t$prog_name --action install\n
\tOptions:
\t\t--action     - Action type {install|update}\n\n
\t\t--interface  - Main network interface name (eth1)\n\n
\t\t--user       - Admin user name (\"aps-admin\")\n\n"
}

function process_opts {
   shortopts="a:i:u:"
   longopts="action:,interface:,user:"

   args=$(getopt -o "$shortopts" -l "$longopts" -n $prog_name -- "$@")

   if [[ $? -ne 0 ]]; then
      print_help
      exit 1
   fi

   eval set -- "$args"

   while true; do
      case "$1" in
         -a|--action)
            action="$2"
            shift 2
            ;;
        -i|--interface)
           interface_name="$2"
           shift 2
           ;;
       -u|--user)
          admin_user="$2"
          shift 2
          ;;
         --)
            shift
            if [ -z "$action" ]; then
               echo "ERROR: action is not specified."
               exit 1
            fi
            break
            ;;
         *)
            echo "ERROR: Unknown option: $1. Please check your command syntax."
            exit 1
            ;;
      esac
   done
}

process_opts "$@"

function fix_privileges {
  chown -R $admin_user $global_conf_dir  &&
  chmod -R a+rX $global_conf_dir         &&
  chmod a+x $global_conf_dir/*.sh
  return $?
}


function configure_dynconfig {
  echo "Deploying dynamic config data"
  mkdir -p /var/spool/f2b-blacklist/add &&
  mkdir -p /var/spool/f2b-blacklist/del &&

  mkdir        $global_conf_dir/config.dynamic &&

  cd $global_conf_dir/config.dynamic      &&
  touch antispam.blacklist-domain.squid   &&
  touch antispam.blacklist-domip.squid    &&
  touch antispam.blacklist-iponly.squid   &&
  touch auth.proxy-user-password.url      &&
  touch auth.userip-proxyip.squid         &&
  touch auth.userip-proxyip.system        &&
  touch auth.userip-proxyip.url           &&
  touch auth.user-password.squid          &&
  touch auth.user-proxyip.squid           &&
  touch changes-ip.trigger                &&
  touch changes-pw.trigger                &&
  touch changes.url                       &&
  touch local_ip_addresses                &&

  install_cmd="install --verbose --mode=644"
  $install_cmd $prog_dir/../system-config/incron.dynconfig.fetch   /etc/incron.d/dynconfig.fetch   &&
  $install_cmd $prog_dir/../system-config/incron.dynconfig.squid   /etc/incron.d/dynconfig.squid   &&
  $install_cmd $prog_dir/../system-config/incron.dynconfig.system  /etc/incron.d/dynconfig.system  &&
  $install_cmd $prog_dir/../system-config/incron.f2b-blacklist     /etc/incron.d/f2b-blacklist

  return $?
}

function action_install {
  require_root
  curr_date=`date "+%Y%m%d-%H%M"`
  exitstatus=0

  global_if_file="$global_conf_dir/interface"
  global_range_file="$global_conf_dir/ip_ranges"

  mkdir $global_conf_dir           &&
  mkdir $global_range_file.archive &&

  mkdir $global_conf_dir/backup    &&
  touch $global_if_file
  exitstatus=$(($exitstatus+$?))

  echo -n $interface_name > $global_if_file               &&
  current_range_file="$global_range_file.archive/$curr_date.server-install"
  ln -s $current_range_file $global_range_file    &&
  touch $current_range_file  &&
  configure_dynconfig &&

  action_update ; exitstatus=$(($exitstatus+$?))

  return $exitstatus
}

function action_update {
  install --mode=755 -t $global_conf_dir $scripts_src_dir/*.sh &&
  fix_privileges
  return $?
}


case "$action" in
   install)
      action_install
      ;;
   update)
      backup_list=`ls $global_conf_dir/*.sh`
      backup_config $global_conf_dir/backup/aps_config_update "$backup_list" &&
      action_update
      ;;
   *)
      echo "ERROR: Unknown action: $action"
      exit 1
      ;;
esac
