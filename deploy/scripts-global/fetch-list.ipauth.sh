#!/bin/bash
#
# Refresh IP authorization list
#
prog_dir=$(readlink -f `dirname $0`)

### Configurable parameters

global_conf_dir="/srv/aps-proxy"
dynconf_dir="$global_conf_dir/config.dynamic"
iplist_file="$dynconf_dir/local_ip_addresses"


url="http://www.example.local/auth-userip-proxyip.txt"
target_file="$dynconf_dir/auth.userip-proxyip.url"

target_squid="$dynconf_dir/auth.userip-proxyip.squid"
target_system="$dynconf_dir/auth.userip-proxyip.system"

function update_if_changed {
  src_file="$1"
  dst_file="$2"
  cmp_flag=0

  # Detect changes
  if [ -e "$dst_file" ]; then
    cmp -s "$src_file" "$dst_file"
    cmp_flag=$?
    [ $cmp_flag -eq 0 ] || cat "$src_file" > "$dst_file"
  else
    cat "$src_file" > "$dst_file"
    return 1
  fi

  return $cmp_flag
}

function parse_ipauth_list {
  local tmp_file=`mktemp`
  infile="$1"

  cat $infile | grep -E "^(([0-9]+\.){3}[0-9]+\s+)([0-9]+\.){3}[0-9]+$" | sort  > $tmp_file
  awk 'NR==FNR {localip[$0];next}  ($2 in localip) { print }' $iplist_file $tmp_file > $infile

  rm $tmp_file
}


### Main actions
tmp_file=`mktemp`

$prog_dir/fetch-list.sh "$url" "$tmp_file"

update_if_changed $tmp_file $target_file

# Extract only local IPs, remove incorrect lines
parse_ipauth_list $tmp_file
update_if_changed $tmp_file $target_squid
update_if_changed $tmp_file $target_system


rm $tmp_file
