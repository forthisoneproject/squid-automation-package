#!/bin/bash
# Squid service checks and restart

# Configurable parameters
squid_conf="/etc/squid/squid.conf"

# Simple "squid is not running" check
if ! service squid status > /dev/null ; then
  echo "Squid service is not running, restarting it."
  service squid restart
  exit 0
fi

# Check if all configured ports are open for listening
conf_ports=`grep -E "^http_port" $squid_conf | wc -l`
open_ports=`netstat -lnpt | grep squid | wc -l`

if [ "$open_ports" -lt "$conf_ports" ]; then
  echo "Number of squid open ports ($open_ports) is less than configured ($conf_ports). Restarting."
  service squid reload
fi

exit 0
