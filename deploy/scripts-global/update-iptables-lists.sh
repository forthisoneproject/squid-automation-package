#!/bin/bash
# Updating current configuration IPTABLES lists
#

# Check if this script has root credentials
if [ $(id -u) -ne "0" ]; then  echo "This script should be executed by root. Aborting."; exit 1; fi

# Global configuration
global_conf_dir="/srv/aps-proxy"

authip_list="$global_conf_dir/config.dynamic/auth.userip-proxyip.system"
whitelist_set="whitelist"

temp_file=`mktemp`
awk '{print $1}' $authip_list | grep -E "^([0-9]+\.){3}[0-9]+$" | sort | uniq > $temp_file


ipset flush $whitelist_set
while read curr_ip ; do
  ipset add $whitelist_set $curr_ip
done < $temp_file

rm $temp_file
