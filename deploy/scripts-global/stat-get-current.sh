#!/bin/bash
# All current stats parser
#

# Configurable parameters
summary_log_period="7"
log_file_path=""
log_file_name=""
sum_ips=false
sum_logins=false
out_sum_ips=""
out_sum_logins=""


# Automatic parameters
prog_name=`basename $0`
prog_dir=$(readlink -f `dirname $0`)

function print_help {
   echo -e "\nExample:\n\n\t$prog_name --log-path /var/log/squid\n
\tOptions:
\t\t--log-path    - path to directory with log files\n
\t\t--log-name    - base name of log files (example: extended.log)\n
\t\t--sum-period  - history depth for analysis (days ago from current)\n
\t\t--sum-ips     - collect summary for IPs\n
\t\t--out-ips     - output file for IPs summary\n
\t\t--sum-logins  - collect summary for logins\n
\t\t--out-logins  - output file for logins summary\n
"
}

function process_opts {
  shortopts="p:n:d:"
  longopts="log-path:,log-name:,sum-period:,sum-ips,sum-logins,out-ips:,out-logins:"

  args=$(getopt -o "$shortopts" -l "$longopts" -n $prog_name -- "$@")

  if [[ $? -ne 0 ]]; then print_help; exit 1; fi

  eval set -- "$args"

  while true; do
    case "$1" in
      -p|--log-path)
        log_file_path="$2"
        shift 2
        ;;
      -n|--log-name)
        log_file_name="$2"
        shift 2
        ;;
      -d|--sum-period)
        summary_log_period="$2"
        shift 2
        ;;
      --sum-ips)
        sum_ips=true
        shift
        ;;
      --sum-logins)
        sum_logins=true
        shift
        ;;
      --out-logins)
        out_sum_logins="$2"
        shift 2
        ;;
      --out-ips)
        out_sum_ips="$2"
        shift 2
        ;;
      --)
        shift
        if $sum_ips && [ -z "$out_sum_ips" ] ; then
          echo "ERROR: output file --out-ips must be specified"
          exit 1
        fi
        if $sum_logins && [ -z "$out_sum_logins" ] ; then
          echo "ERROR: output file --out-logins must be specified"
          exit 1
        fi

        break
        ;;
      *)
        echo "ERROR: Unknown option: $1. Please check your command syntax."
        exit 1
        ;;
    esac
  done
}

process_opts "$@"

# Performing actions
tmp_file=`mktemp`
summary_params="--period $summary_log_period"
exitstatus=0
[ -z "$log_file_path" ] || summary_params="$summary_params --log-path $log_file_path"
[ -z "$log_file_name" ] || summary_params="$summary_params --log-name $log_file_name"

if $sum_ips ; then
  $prog_dir/stat-summary-ips.sh $summary_params  \
    > $tmp_file &&
  cat $tmp_file > $out_sum_ips

  exitstatus=$(($exitstatus+$?))
fi

if $sum_logins ; then
  $prog_dir/stat-summary-logins.sh $summary_params  \
    > $tmp_file &&
  cat $tmp_file > $out_sum_logins
  exitstatus=$(($exitstatus+$?))
fi

rm $tmp_file
exit $exitstatus
