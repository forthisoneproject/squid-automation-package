#!/bin/bash

# Global configuration
global_conf_dir="/srv/aps-proxy"

# Network interface name
net_interface=`cat $global_conf_dir/interface`
net_interface=${1:-$net_interface}

check_host="a.root-servers.net"

if [ -z "$net_interface" ]; then
   echo "ERROR: Specify interface name in the command line."
   exit 1
fi

iplist=`ip addr show dev $net_interface | grep "inet " | awk '{print $2}' | cut -d'/' -f1`

total_ip=0
working_ip=0
failed_ip=0


echo "Starting ip addresses check."
results_file=`mktemp`

function ip_check {
  srcIP="$1"
  ping -q -c 3 -i 0.5 -I $srcIP $check_host > /dev/null
  if [ $? -eq 0 ]; then
    echo $srcIP OK >> $results_file
  else
    echo $srcIP FAILED >> $results_file
  fi
}

declare -a pid_list

while read currIP ; do
	total_ip=$(($total_ip+1))
  ip_check $currIP &
  pid_list=("${pid_list[@]}" $!)
done <<< "$iplist"

function wait_exec {
  while true; do
    exit_flag=true
    for i in ${!pid_list[*]} ; do
      if [ -e "/proc/${pid_list[$i]}" ]; then
        exit_flag=false
        break
      fi
    done
    if $exit_flag ; then break; fi
    sleep 1
  done
}

wait_exec
failed_ip=`grep FAILED $results_file | wc -l`
working_ip=`grep OK $results_file | wc -l`

grep FAILED $results_file
echo ""
echo "==== Summary ===="
echo "Total: $total_ip  OK: $working_ip  Failed: $failed_ip"

rm $results_file
