#!/bin/bash
#
# Squid server IP ranges configuration
#

# Check if this script has root credentials
if [ $(id -u) -ne "0" ]; then  echo "This script should be executed by root. Aborting."; exit 1; fi

### Configuration variables section
# Input all server's parameters here

# Global configuration
global_conf_dir="/srv/aps-proxy"
backup_dir="$global_conf_dir/backup"

range_file="$global_conf_dir/ip_ranges"

squid_dir="/etc/squid"
squid_range_dir="$squid_dir/ip-ranges.dynamic"
squid_range_conf="$squid_range_dir/ip-ranges.conf"

# Network interface name
net_interface=`cat $global_conf_dir/interface`


# IP ranges definitions
# Usage: new_range  SUBNET  START  END  MASK
#   SUBNET - first 3 numbers of subnet, without trailing dot. Example: 1.2.3
#   START  - starting address in range
#   MASK   - netmask for the segment

### Technical initialization section
# Do not edit anything here
declare -a R_NET
declare -a R_MASK
declare -a R_START
declare -a R_END

function new_range {
      R_NET=("${R_NET[@]}"   $1)
    R_START=("${R_START[@]}" $2)
      R_END=("${R_END[@]}"   $3)
     R_MASK=("${R_MASK[@]}"  $4)
}

ip_ranges_list=`cat $range_file | sed -r 's/^([0-9\.]+)\.([0-9]+)\s+[0-9\.]+\.([0-9]+)\s+(.+)$/\1 \2 \3 \4/g'`

if [ -z "$ip_ranges_list" ]; then
   echo "ERROR: IP ranges list is empty or has wrong format."
   exit 1
fi

while read range_base range_start range_end range_mask ; do
   new_range $range_base $range_start $range_end $range_mask
done <<< "$ip_ranges_list"

# Configuration files paths
ifcfg_range_base="/etc/sysconfig/network-scripts/ifcfg-$net_interface-range"

PATH_HOME=`dirname $0`

# Backup current config
function backup_config {
   curr_date=`date "+%Y%m%d-%H%M"`
   bak_file="$backup_dir/range-reconfig.$curr_date.tar"

   echo "Creating backup of current configuration in file:"
   echo $bak_file

   # Create backup
   sed -r "s/^\///" <<BAK_LIST | tar --create -C / --file $bak_file --files-from -
/etc/squid
`ls /etc/sysconfig/network-scripts/ifcfg-*`
BAK_LIST

   # Check if backup was created successfully
   if [ $? -ne "0" ]; then
     echo "ERROR: Backup creation failed. Abort."
     exit 1
   fi

}

backup_config

### IP range configuration (System)


# Create interface configuration file
function ip_range_config {
    RFILE=$1
    RSTART=$2
    REND=$3
    RMASK=$4
    RNUM=$5

cat <<__RANGEFILE__ > $RFILE
IPADDR_START=$RSTART
IPADDR_END=$REND
CLONENUM_START=$RNUM
NETMASK=$RMASK
ARPCHECK="no"
__RANGEFILE__

}

# Create Squid configuration file
function squid_ip_range_config {
   BASE_NET=$1
   IP_START=$2
   IP_END=$3

   OUT_FILE=$4

   for i in `seq $IP_START $IP_END`
   do
      CURR_IP="$BASE_NET.$i"
      ACL_NAME="input_ip_$CURR_IP"
      echo "acl $ACL_NAME myip $CURR_IP" >> $OUT_FILE
      echo "tcp_outgoing_address $CURR_IP $ACL_NAME" >> $OUT_FILE
   done
}

# Clean old configuration
find `dirname $ifcfg_range_base` -maxdepth 1 -name `basename $ifcfg_range_base`* -delete

# Init indexes
RANGE_INDEX=0
RANGE_NUM=1
for i in ${!R_NET[*]}
do
    # Convert numeric parameters to IPs
    RANGE_START="${R_NET[$i]}.${R_START[$i]}"
      RANGE_END="${R_NET[$i]}.${R_END[$i]}"
     RANGE_MASK="${R_MASK[$i]}"
    # Current range configuration file name
    current_file="$ifcfg_range_base$RANGE_INDEX"

    # Run configuration procedures
    ip_range_config $current_file $RANGE_START $RANGE_END $RANGE_MASK $RANGE_NUM

    # Prepare indexes for next iteration
    RANGE_INDEX=$(($RANGE_INDEX+1))
    RANGE_NUM=$(($RANGE_NUM+${R_END[$i]}-${R_START[$i]}+1))
done


### IP range configuration (Squid)

# Clean old ranges config
rm $squid_range_dir/ip-range*.conf

# Generate new configuration
echo "# Originating IP address configurations" > $squid_range_conf

# Init indexes
RANGE_INDEX=0
for i in ${!R_NET[*]}
do
    # Generate source-ip config for current IP range
    current_conf="$squid_range_dir/ip-range.$RANGE_INDEX.conf"
    echo "" > $current_conf
    squid_ip_range_config ${R_NET[$i]} ${R_START[$i]} ${R_END[$i]} $current_conf
    echo "include $current_conf" >> $squid_range_conf
    # Prepare indexes for next iteration
    RANGE_INDEX=$(($RANGE_INDEX+1))
done

systemctl restart network
systemctl reload squid
$global_conf_dir/get-system-ip-list.sh
