#!/bin/bash
# Send network statistics to main server
#

# configurable parameters
url_api='http://www.example.local/stats-input.php'
stat_file="/var/spool/squid/network_statistics"
stat_interval=359
curr_date=`date +%s`
servername=`hostname`
serverip=`ip route get 1 | awk '{print $NF;exit}'`

if [ ! -r "$stat_file" ]; then
  echo "ERROR: input file not readable: $stat_file"
  exit 1
fi

# get statistics
min_date=$(($curr_date-$stat_interval))
stat_data=`awk -v min_date=$min_date '$1 >= min_date {print}' $stat_file`

sum_stat=0
while read cdate net_in net_out ; do
  sum_stat=$(($sum_stat+$net_in+$net_out))
done <<< "$stat_data"

curl --silent -o /dev/null "$url_api?serverName=$servername&serverIp=$serverip&counter=$sum_stat"
