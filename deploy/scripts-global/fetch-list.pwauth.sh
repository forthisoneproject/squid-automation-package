#!/bin/bash
#
# Refresh user/password authorization list
#
prog_dir=$(readlink -f `dirname $0`)

### Configurable parameters

global_conf_dir="/srv/aps-proxy"
dynconf_dir="$global_conf_dir/config.dynamic"


url="http://www.example.local/auth-user-password.txt"
target_file="$dynconf_dir/auth.proxy-user-password.url"

target_squid_pw="$dynconf_dir/auth.user-password.squid"
target_squid_proxy="$dynconf_dir/auth.user-proxyip.squid"

function update_if_changed {
  src_file="$1"
  dst_file="$2"
  cmp_flag=0

  # Detect changes
  if [ -e "$dst_file" ]; then
    cmp -s "$src_file" "$dst_file"
    cmp_flag=$?
    [ $cmp_flag -eq 0 ] || cat "$src_file" > "$dst_file"
  else
    cat "$src_file" > "$dst_file"
    return 1
  fi

  return $cmp_flag
}

function parse_user_list {
  awk '{print $2,$3}' $target_file | sort | uniq  > $target_squid_pw
  awk '{print $2,$1}' $target_file | sort | uniq  > $target_squid_proxy
}

### Main actions
tmp_file=`mktemp`

$prog_dir/fetch-list.sh "$url" "$tmp_file"

# Add superuser account
# echo "0.0.0.0:aps-admin:password" >> $tmp_file

update_if_changed $tmp_file $target_file ||
parse_user_list

rm $tmp_file
