#!/bin/bash
# Squid statistics analyzer
# by-login summary
#
# Requires custom log format as input:
# date clientIP clientPort proxyIP proxyPort STATUS/CODE reply_size req_size clientName
#   1     2         3         4        5          6           7         8        9
# logformat extended %ts.%03tu %>a %>p %la %lp %Ss/%03>Hs %<st %>st %un


# Configurable parameters
log_file_path="/var/log/squid"
log_file_name="extended.log"
log_period="7"

# Automatic parameters
prog_name=`basename $0`
prog_dir=$(readlink -f `dirname $0`)

function print_help {
   echo -e "\nExample:\n\n\t$prog_name --log-path /var/log/squid\n
\tOptions:
\t\t--log-path    - path to directory with log files\n
\t\t--log-name    - base name of log files (example: extended.log)\n
\t\t--period      - history depth for analysis (days ago from current)\n
"
}

function process_opts {
  shortopts="p:n:d:"
  longopts="log-path:,log-name:,period:"

  args=$(getopt -o "$shortopts" -l "$longopts" -n $prog_name -- "$@")

  if [[ $? -ne 0 ]]; then print_help; exit 1; fi

  eval set -- "$args"

  while true; do
    case "$1" in
      -p|--log-path)
        log_file_path="$2"
        shift 2
        ;;
      -n|--log-name)
        log_file_name="$2"
        shift 2
        ;;
      -d|--period)
        log_period="$2"
        shift 2
        ;;
      --)
        shift
        if [ -z "$log_file_path" ]; then
           echo "ERROR: Log files path must be specified."
           exit 1
        fi
        if [ ! -d "$log_file_path" ]; then
           echo "ERROR: Log files path is not accessible: $log_file_path"
           exit 1
        fi

        if [ -z "$log_file_name" ]; then
           echo "ERROR: Log file name must be specified."
           exit 1
        fi

        find "$log_file_path" -name "${log_file_name}*" > /dev/null
        if [ $? -ne 0 ]; then
          echo "ERROR: Log files not found: ${log_file_name}*"
          exit 1
        fi

        break
        ;;
      *)
        echo "ERROR: Unknown option: $1. Please check your command syntax."
        exit 1
        ;;
    esac
  done
}

process_opts "$@"

# Concatenating all log files, including gzipped
function concat_logs {
  find "$log_file_path" -name "${log_file_name}*" -a \( ! -name "*.gz" \) -exec cat {} +
  find "$log_file_path" -name "${log_file_name}*.gz"  -exec zcat {} +
}

# Calculate minimal unixtime
days_ago=`date --date="- $log_period days" "+%s"`

# Prepare script file
script_file=`mktemp`
cat > $script_file <<AWK_SCRIPT
\$1 >= mindate && \$9 != "-" {
  sum_key = \$9
  sum_bytes[sum_key] += \$7+\$8
  sum_req[sum_key] += 1
  proxyips[sum_key,\$4]+=1
}

END {
  for (i in sum_bytes) {
    proxynum = 0
    for (comb in proxyips) {
      split(comb,sepr,SUBSEP)
      if (sepr[1] == i) proxynum += 1
    }

    print i,sum_bytes[i],sum_req[i],proxynum
  }
}
AWK_SCRIPT

# Analyze logs
concat_logs | awk -v mindate=$days_ago -f $script_file

# Remove temporary file
rm $script_file
