#!/bin/bash
#
# Refresh lists for user's IP authentication
#
prog_dir=$(readlink -f `dirname $0`)

### Configurable parameters

global_conf_dir="/srv/aps-proxy"
squid_dir="/etc/squid"
primary_ip=`ip route get 1 | awk '{print $NF;exit}'`
url_key="4e5b4424896cc7977a45bd394dd6aab6"

ip_ranges_url="http://www.example.local/myip-list.$primary_ip.txt"
ip_ranges_archive="$global_conf_dir/ip_ranges.archive"
ip_ranges="$global_conf_dir/ip_ranges"

ip_ranges_script="$global_conf_dir/range-reconfig.sh"

temp_file=`mktemp`

# Fetch list from URL
function fetchlist {
   url=$1
   outfile=$2

   tmp_file1=`mktemp`
   tmp_file2=`mktemp`

   wget -q -O $tmp_file1 $url

   # Check fetching for errors and unpack list
   if [ $? -eq 0 ]; then
      gunzip -c $tmp_file1 | tr -d '\r' > $tmp_file2
   else
      echo "ERROR while fetching URL."
      rm $tmp_file1
      rm $tmp_file2
      exit 1
   fi

   # Check for zero-sized list
   FILESIZE=$(stat -c%s "$tmp_file2")
   if [ $FILESIZE -eq 0 ]; then
      echo "ERROR: received empty list"
      rm $tmp_file1
      rm $tmp_file2
      exit 1
   fi

   # Copy fetched list to final destination
   cat $tmp_file2 | sort > $outfile

   rm $tmp_file1
   rm $tmp_file2
}

function syntax_check {
   test_file=$1

   grep "0.0.0.0" $test_file
   if [ $? -eq 0 ]; then
      return 1
   fi

   return 0
}

# Check if the ranges configuration has changed and update it
function update_range {
   ip_ranges_new=$1

   md5_curr=`cat $ip_ranges | md5sum`
   md5_new=`cat $ip_ranges_new | md5sum`

   if [ "$md5_curr" != "$md5_new" ]; then
      echo "IP ranges configuration update received, reconfiguring."
      curr_date=`date "+%Y%m%d-%H%M"`
      ip_ranges_newfile="$ip_ranges_archive/$curr_date.from-url"

      cat $ip_ranges_new > $ip_ranges_newfile
      rm $ip_ranges.last
      mv $ip_ranges $ip_ranges.last
      ln -s $ip_ranges_newfile $ip_ranges

      $ip_ranges_script --reload
   fi
}

# Fetch list from server
fetchlist $ip_ranges_url $temp_file
syntax_check $temp_file
if [ $? -eq 0 ]; then
   update_range $temp_file
fi

rm $temp_file
