#!/bin/bash
# Script for uploading stats via cron

# Configurable parameters
upload_path="/var/spool/squid/stat-upload"
remote_host="169.254.0.36"
remote_login="proxy-upload"
identity_file="/srv/aps-proxy/ssh-auth-stat"
remote_path="/stat-summary"

# Automatic parameters
prog_name=`basename $0`
prog_dir=$(readlink -f `dirname $0`)

# Perform actions

find $upload_path -type f -exec $prog_dir/sftp-upload.sh \
  --quiet \
  --file {} \
  --delete  \
  --host $remote_host \
  --login $remote_login \
  --identity $identity_file \
  --remote-path $remote_path \
  \;

exit $?
