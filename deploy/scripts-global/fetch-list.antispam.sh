#!/bin/bash
#
# Refresh user/password authorization list
#
prog_dir=$(readlink -f `dirname $0`)

### Configurable parameters

global_conf_dir="/srv/aps-proxy"
dynconf_dir="$global_conf_dir/config.dynamic"


  url_domlist='http://www.example.local/blacklist-domains.txt'
url_domiplist='http://www.example.local/blacklist-ip.txt'
   url_iponly='http://www.example.local/blacklist-ip-add.txt'


  target_domlist="$dynconf_dir/antispam.blacklist-domain.squid"
target_domiplist="$dynconf_dir/antispam.blacklist-domip.squid"
   target_iponly="$dynconf_dir/antispam.blacklist-iponly.squid"



function update_if_changed {
  src_file="$1"
  dst_file="$2"
  cmp_flag=0

  # Detect changes
  if [ -e "$dst_file" ]; then
    cmp -s "$src_file" "$dst_file"
    cmp_flag=$?
    [ $cmp_flag -eq 0 ] || cat "$src_file" > "$dst_file"
  else
    cat "$src_file" > "$dst_file"
    return 1
  fi

  return $cmp_flag
}

### Main actions
tmp_file=`mktemp`

$prog_dir/fetch-list.sh "$url_domlist" "$tmp_file"
update_if_changed $tmp_file $target_domlist

$prog_dir/fetch-list.sh "$url_domiplist" "$tmp_file"
update_if_changed $tmp_file $target_domiplist

$prog_dir/fetch-list.sh "$url_iponly" "$tmp_file"
update_if_changed $tmp_file $target_iponly

rm $tmp_file
