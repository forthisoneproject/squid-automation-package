#!/bin/bash
# Updating current configuration of connection limiter
#

# Check if this script has root credentials
if [ $(id -u) -ne "0" ]; then  echo "This script should be executed by root. Aborting."; exit 1; fi

# Global configuration
global_conf_dir="/srv/aps-proxy"

iptable_name="user-conn-limit"
conn_limit_num="100"

# Backup file
bak_file=`mktemp`

# Default ip-list file
iplist_file="$global_conf_dir/config.dynamic/local_ip_addresses"

# Backup current configuration
iptables-save > $bak_file

function restore_backup {
  echo "Restoring previous configuration."
  cat $bak_file | iptables-restore
  cp  $bak_file /etc/sysconfig/iptables
}

function save_current_config {
  iptables-save > /etc/sysconfig/iptables
}

# Process IP list and fill the iptables
function fill_iptable {
  iptables -F $iptable_name
  iptables -A $iptable_name -j ACCEPT

  rule_template="-I $iptable_name -j DROP -p tcp --syn -m connlimit --connlimit-above $conn_limit_num --connlimit-mask 32"
  while read curr_ip ; do
    iptables $rule_template --destination $curr_ip
    [ $? -eq 0 ] || return 1
  done < $iplist_file

}

fill_iptable &&
save_current_config

[ $? -eq 0 ] || restore_backup

rm $bak_file
