#!/bin/bash
# Generic list downloader

function is_compressed {
  fname="$1"
  file $fname | grep gzip > /dev/null
  return $?
}

# Fetch list from URL
function fetchlist {
   url=$1
   outfile=$2

   tmp_file1=`mktemp`
   tmp_file2=`mktemp`

   wget -q --header="accept-encoding: gzip" -O $tmp_file1 $url

   # Check fetching for errors and unpack list
   if [ $? -eq 0 ]; then
     if is_compressed $tmp_file1 ; then
       gunzip -c $tmp_file1 > $tmp_file2
     else
       cat $tmp_file1 > $tmp_file2
     fi
   else
      echo "ERROR while fetching URL"
      rm $tmp_file1
      rm $tmp_file2
      return 1
   fi

   # Check for zero-sized list
   FILESIZE=$(stat -c%s "$tmp_file2")
   if [ $FILESIZE -eq 0 ]; then
      echo "ERROR: received empty list"
      rm $tmp_file1
      rm $tmp_file2
      return 1
   fi

   # Copy fetched list to final destination
   cat $tmp_file2 | tr -d '\r' | sort | uniq > $outfile
   exitstatus=$?

   rm $tmp_file1
   rm $tmp_file2

   return $exitstatus
}

# Fetch list from server
fetchlist "$1" "$2"
