#!/bin/bash
# Custom wrapper for uploading a single file

# Configurable parameters
src_file=""
remote_host=""
remote_login=""
remote_path=""
identity_file=""
quiet_mode=false
src_delete=false

# Automatic parameters
prog_name=`basename $0`
prog_dir=$(readlink -f `dirname $0`)

function print_help {
   echo -e "\nExample:\n\n\t$prog_name --file /path/to/file --host 1.2.3.4 --login upload --identity /path/to/id-file\n
\tOptions:
\t\t--file          - path to source file to upload\n
\t\t--quiet         - Do not produce output even when fail\n
\t\t--delete        - Delete source file after successful upload\n
\t\t--host          - remote host name\n
\t\t--login         - remote login name\n
\t\t--identity      - SSH private key for authentication\n
\t\t--remote-path   - path on remote server\n
"
}

function qecho {
  $quiet_mode || echo "$@"
}

function process_opts {
  shortopts="qh"
  longopts="help,file:,quiet,host:,login:,identity:,remote-path:,delete"

  if [ -z "$1" ]; then print_help; exit 1; fi

  args=$(getopt -o "$shortopts" -l "$longopts" -n $prog_name -- "$@")

  if [ $? -ne 0 ]; then print_help; exit 1; fi

  eval set -- "$args"

  while true; do
    case "$1" in
      -h|--help)
        print_help
        exit 0
        ;;
      --file)
        src_file="$2"
        shift 2
        ;;
      -q|--quiet)
        quiet_mode=true
        shift
        ;;
      --delete)
        src_delete=true
        shift
        ;;
      --host)
        remote_host="$2"
        shift 2
        ;;
      --login)
        remote_login="$2"
        shift 2
        ;;
      --identity)
        identity_file="$2"
        shift 2
        ;;
      --remote-path)
        remote_path="$2"
        shift 2
        ;;
      --)
        shift
        if [ -z "$src_file" ]; then
          qecho "ERROR: source file is not specified"
          exit 1
        fi
        if [ ! -r "$src_file" ]; then
          qecho "ERROR: source file not accessible: $src_file"
          exit 1
        fi

        if [ -z "$remote_host" ]; then
          qecho "ERROR: remote host name not specified"
          exit 1
        fi
        if [ -z "$remote_login" ]; then
          qecho "ERROR: remote login name not specified"
          exit 1
        fi
        if [ -z "$identity_file" ] ; then
          qecho "ERROR: identity file must be specified"
          exit 1
        fi
        if [ ! -r "$identity_file" ] ; then
          qecho "ERROR: identity file not accessible: $identity_file"
          exit 1
        fi

        break
        ;;
      *)
        echo "ERROR: Unknown option: $1. Please check your command syntax."
        exit 1
        ;;
    esac
  done
}

process_opts "$@"

# Performing actions
exitstatus=0
batch_file=`mktemp`
ssh_config=`mktemp`
tmp_out_file=`mktemp`

[ -z "$remote_path" ] || echo "cd $remote_path" > $batch_file
cat >> $batch_file <<SFTP_BATCH
put $src_file
quit
SFTP_BATCH

# Create SSH config file
cat > $ssh_config <<SSH_CONFIG
HashKnownHosts no
UserKnownHostsFile /dev/null
Host $remote_host
  User $remote_login
  IdentityFile $identity_file
  StrictHostKeyChecking no
SSH_CONFIG

sftp -F $ssh_config -b $batch_file $remote_host \
  >> $tmp_out_file 2>> $tmp_out_file
exitstatus=$?

if [ "$exitstatus" -eq 0 ]; then
  $src_delete && rm $src_file
else
  sftp_out=`cat $tmp_out_file`
  qecho "File upload failed. SFTP output below"
  qecho "$sftp_out"
fi

rm $batch_file $tmp_out_file $ssh_config

exit $exitstatus
