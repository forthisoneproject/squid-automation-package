#!/bin/bash
#
# Detect changes in authentication lists
#
prog_dir=$(readlink -f `dirname $0`)

### Configurable parameters

global_conf_dir="/srv/aps-proxy"
dynconf_dir="$global_conf_dir/config.dynamic"

url="http://www.example.local/auth-changes.txt"
target_file="$dynconf_dir/changes.url"
target_pwlist="$dynconf_dir/changes-pw.trigger"
target_iplist="$dynconf_dir/changes-ip.trigger"

function update_if_changed {
  src_file="$1"
  dst_file="$2"
  cmp_flag=0

  # Detect changes
  if [ -e "$dst_file" ]; then
    cmp -s "$src_file" "$dst_file"
    cmp_flag=$?
    [ $cmp_flag -eq 0 ] || cat "$src_file" > "$dst_file"
  else
    cat "$src_file" > "$dst_file"
    return 1
  fi

  return $cmp_flag
}

### Main actions
tmp_file=`mktemp`

$prog_dir/fetch-list.sh "$url" $target_file

grep "plist.pw" $target_file > $tmp_file
update_if_changed $tmp_file $target_pwlist

grep "plist.ip" $target_file > $tmp_file
update_if_changed $tmp_file $target_iplist

rm $tmp_file
