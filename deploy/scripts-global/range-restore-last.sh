#!/bin/bash
#
# Refresh lists for user's IP authentication
#
prog_dir=$(readlink -f `dirname $0`)

### Configurable parameters

global_conf_dir="/srv/aps-proxy"

ip_ranges_archive="$global_conf_dir/ip_ranges.archive"
ip_ranges="$global_conf_dir/ip_ranges"

ip_ranges_script="$global_conf_dir/range-reconfig.sh"

restore_this_range=${1:-"$ip_ranges.last"}


# Restore last ranges configuration
function restore_range {
   ip_ranges_new=$1
   if [ ! -e "$ip_ranges_new" ]; then
      echo "ERROR: data file does not exist: $ip_ranges_new"
      echo "Here's the list of possible values:"
      ls $ip_ranges_archive
      exit 1
   fi

   echo "Restoring IP ranges configuration from $ip_ranges_new."
   cat $ip_ranges_new

   curr_date=`date "+%Y%m%d-%H%M"`
   ip_ranges_newfile="$ip_ranges_archive/$curr_date.restore"

   cat $ip_ranges_new > $ip_ranges_newfile
   rm $ip_ranges.last
   mv $ip_ranges $ip_ranges.last
   ln -s $ip_ranges_newfile $ip_ranges

   $ip_ranges_script --reload
}

# Perform action
if [ ! -e "$restore_this_range" ]; then
   restore_this_range="$ip_ranges_archive/$restore_this_range"
fi

restore_range $restore_this_range
