#!/bin/bash
# Gather network usage statistics
# Output format:
# time_epoch,IpExtInOctets,IpExtOutOctets

stat_file="/var/spool/squid/network_statistics"
date=`date +%s`
netstat=`nstat -z`

net_in=`echo "$netstat" | sed -n -r 's/IpExtInOctets\s+([0-9]+)\s.+$/\1/ p'`
net_out=`echo "$netstat" | sed -n -r 's/IpExtOutOctets\s+([0-9]+)\s.+$/\1/ p'`

echo $date $net_in $net_out >> $stat_file
