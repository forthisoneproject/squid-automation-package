#!/bin/bash
# Script for gathering stats via cron

# Configurable parameters
hostname=`hostname`
save_path="/var/spool/squid"
upload_path="/var/spool/squid/stat-upload"
archive_path="/var/spool/squid/stat-archive"
log_period=7
log_path="/var/log/squid"
log_name="extended.log"
curr_date=`date +%s`

# Automatic parameters
prog_name=`basename $0`
prog_dir=$(readlink -f `dirname $0`)

# Perform actions

$prog_dir/stat-get-current.sh --sum-period $log_period \
  --sum-ips     --out-ips     $save_path/stat-summary.ips \
  --sum-logins  --out-logins  $save_path/stat-summary.logins \
  --log-path $log_path \
  --log-name $log_name \
  &&

# Make copies
cp $save_path/stat-summary.ips    $upload_path/$hostname.ips      &&
cp $save_path/stat-summary.logins $upload_path/$hostname.logins   &&
cp $save_path/stat-summary.ips    $archive_path/stat-summary.ips.$curr_date      &&
cp $save_path/stat-summary.logins $archive_path/stat-summary.logins.$curr_date

exit $?
