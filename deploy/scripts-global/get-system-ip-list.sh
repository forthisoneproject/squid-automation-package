#!/bin/bash
#
# Creating a list of current system IPs
#

# Global configuration
global_conf_dir="/srv/aps-proxy"

# Network interface name
net_interface=`cat $global_conf_dir/interface`

# Default save file
iplist_file=${1:-"$global_conf_dir/config.dynamic/local_ip_addresses"}

ip addr show dev $net_interface | grep "inet " | awk '{print $2}' | cut -d'/' -f1 > $iplist_file
