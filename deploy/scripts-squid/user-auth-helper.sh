#!/bin/bash
# User IP authentication helper for Squid
#

### Configurable parameters
AUTH_FILE="$1"

if [ ! -r "$AUTH_FILE" ]; then
  echo "Data file is not readable: $AUTH_FILE"
  exit 1
fi

function check_user_auth {
  userName="$1"
  userPwd="$2"

  awk_script="{ if (\$1 == \"$userName\" && \$2 == \"$userPwd\") exit 100; }"
  awk "$awk_script" $AUTH_FILE
  return $?
}

while read userName userPwd; do
  check_user_auth "$userName" "$userPwd"
  exitstatus=$?

  if [ $exitstatus -eq 100 ] ; then
    echo "OK"
  else
    echo "ERR"
  fi
done
