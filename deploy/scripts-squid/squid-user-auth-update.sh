#!/bin/bash
#
# Refresh lists for user's password authentication
#

### Configurable parameters

SQUID_USERCONF_DIR="/etc/squid/authentication.dynamic"

url_userpw="http://www.example.local/user-password.txt"

USERS_FILE="$SQUID_USERCONF_DIR/users.txt"

squid_user_password="$SQUID_USERCONF_DIR/user-password.txt"
squid_user_myip="$SQUID_USERCONF_DIR/user-myip.txt"

function is_compressed {
  fname="$1"
  file $fname | grep gzip > /dev/null
  return $?
}

# Fetch list from URL
function fetchlist {
   url=$1
   outfile=$2

   tmp_file1=`mktemp`
   tmp_file2=`mktemp`

   wget -q --header="accept-encoding: gzip" -O $tmp_file1 $url

   # Check fetching for errors and unpack list
   if [ $? -eq 0 ]; then
      if is_compressed $tmp_file1 ; then
        gunzip -c $tmp_file1 > $tmp_file2
      else
        cat $tmp_file1 > $tmp_file2
      fi
   else
      echo "ERROR while fetching URL."
      rm $tmp_file1
      rm $tmp_file2
      exit 1
   fi

   # Check for zero-sized list
   FILESIZE=$(stat -c%s "$tmp_file2")
   if [ $FILESIZE -eq 0 ]; then
      echo "ERROR: received empty list"
      rm $tmp_file1
      rm $tmp_file2
      exit 1
   fi

   # Copy fetched list to final destination
   cat $tmp_file2 | tr -d '\r' > $outfile

   rm $tmp_file1
   rm $tmp_file2
}

# Fetch list from server
fetchlist "$url_userpw" $USERS_FILE

awk -F ':' '{print $2,$3}' $USERS_FILE | sort | uniq > $squid_user_password
awk -F ':' '{print $2,$1}' $USERS_FILE > $squid_user_myip
