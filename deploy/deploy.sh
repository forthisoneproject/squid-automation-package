#!/bin/bash
#
# Squid server initial setup
#

### Configuration variables section
# Input all server's parameters here

# Installation LOG file
STEP_LOG="/root/aps-proxy-install.log"
echo -n > $STEP_LOG

# Network interface name
srv_if=`ip route get 1 | awk '{print $5;exit}'`

# Primary IP
netif_conf="/etc/sysconfig/network-scripts/ifcfg-$srv_if"
srv_ip=`ip route get 1 | awk '{print $NF;exit}'`

# Server name
srv_name=${1:-"proxy-server-NEW"}
server_timezone="UTC"

# OS detection parameters
os_release_file="/etc/centos-release"
os_release_string="release 7"
os_release_strict=true

# Configuration files paths
global_conf_dir="/srv/aps-proxy"
admin_user="aps-admin"

          squid_dir="/etc/squid"
    squid_main_conf="$squid_dir/squid.conf"
 squid_ipranges_dir="$squid_dir/ip-ranges.dynamic"

# Automatic parameters

prog_name=`basename $0`
prog_dir=$(readlink -f `dirname $0`)

lib_file="$prog_dir/modules/_lib.sh"
if [ ! -e "$lib_file" ]; then
  echo "ERROR: can't load functions library $lib_file"
  exit 1
fi
. $lib_file

require_root

bak_files_list="/etc"

# Setting time zone
function set_timezone_centos7 {
  local timezone="$1"
  timedatectl set-timezone $timezone
  return $?
}

# OS version warning
function os_release_warning {
   echo -e "\t\033[31mThis script is thorougly tested for CentOS 7 only. Won't continue without explicit permission.\033[0m"
   if $os_release_strict ; then
      echo -e "\tTo force installation set \"os_release_strict\" variable to \"false\"."
      return 1
   else
      echo -e "\t\033[1m\033[41mOS release strict check disabled, will continue on your own risk.\033[0m"
      echo -e "\t\033[1m\033[41mWaiting for 10 seconds to continue, \033[1;33mpress CTRL+C to abort...\033[0m"
      sleep 10
      return 0
   fi

   echo -e "ERROR: unknown OS check error."
   return 1
}

# System OS checks
function check_os_release {
   if [ ! -e "$os_release_file" ]; then
      release_autodetect=`ls /etc/*-release | head -n 1`
      echo -n "ERROR: Wrong OS release detected"
      [ -r "$release_autodetect" ] && echo -n ": " `cat $release_autodetect`
      echo

      os_release_warning
      return $?
   fi

   grep -q "$os_release_string" $os_release_file
   if [ $? -ne 0 ]; then
      echo "ERROR: Wrong OS version detected: " `cat $os_release_file`

      os_release_warning
      return $?
   fi

   return 0
}


# Network configuration sanity checks
function net_config_sanity {
   if [ ! -r "$netif_conf" ]; then
      echo "ERROR: Interface $srv_if configuration file is not accessible."
      return 1
   fi

   # Read data from config file
   . $netif_conf
   net_conf_data=`cat $netif_conf`

   if [ "$BOOTPROTO" != "static" ] && [ "$BOOTPROTO" != "none" ] ; then
      echo "ERROR: Network configuration protocol is \"$BOOTPROTO\", should be \"static\". Please review your interface configuration."
      #return 2
   fi

   # Setting new values
   echo "$net_conf_data" | grep -v "NM_CONTROLLED" > $netif_conf &&
   echo 'NM_CONTROLLED="no"' >> $netif_conf
   [ $? -eq 0 ] || return 1

   if rpm -q NetworkManager >/dev/null 2>&1; then
     systemctl disable NetworkManager
     systemctl stop NetworkManager
   fi

   return $?
}


### Hostname configuration
function configure_hostname {
  hostnamectl set-hostname $1
  return $?
}

### Admin tools installation
function install_admin_tools {
  yum -y install      \
    pciutils          \
    net-tools         \
    mc                \
    nano              \
    screen            \
    ethtool           \
    htop              \
    wget              \
    unzip             \
    rsync             \
    python-argparse
  return $?
}

### DNS configuration
function configure_dns {

  cat > /etc/resolv.conf <<EOF
nameserver 208.67.222.222
nameserver 208.67.220.220
nameserver 8.8.8.8
nameserver 8.8.4.4
EOF

  return $?
}

### Global performance tweaks
function tweak_global_performance {
  install --verbose --mode=644 \
    $prog_dir/system-config/sysctl.conf \
    /etc/sysctl.d/10-proxy-perf.conf

   return $?
}

## IPTABLES
function configure_iptables {
   cat $prog_dir/system-config/iptables > /etc/sysconfig/iptables &&
   systemctl restart iptables &&
   systemctl enable iptables
   return $?
}

## ipset install
function install_ipset {
  yum -y install ipset &&
  install -m 644 $prog_dir/system-config/ipset /etc/sysconfig/

  return $?
}

### Squid installation
function squid_install {
   echo "Downloading squid, please wait."
   yum -q -y install squid &&

   # Run squid at startup
   systemctl enable squid

   return $?
}


# Detect if SELinux is enabled on this host
function detect_selinux {
   sestatus | grep "SELinux status" | grep enabled > /dev/null
   return $?
}

# Configure SELINUX
function configure_squid_selinux {
   detect_selinux
   if [ $? -ne 0 ]; then
      echo -e "\n\n\033[1;33m"
      echo -e "SELinux is disabled on this host, skipping this part of configuration."
      echo -e "Note: if you're to enable SELinux in the future you'll need to change proxy configuration too."
      echo -e "\033[0m\n\n"
      return 0
   fi

   # Install management utilities
   echo "Downloading SELinux tools package, please wait."
   yum -q -y install policycoreutils-python
   if [ $? -ne 0 ]; then
     echo "SELinux tools install failed"
     return 1
   fi

   exitstatus=0
   while read scr_name ; do
      helper_path="$squid_dir/scripts/$scr_name"
      semanage fcontext --add --type squid_exec_t $helper_path &&
      restorecon $helper_path ; exitstatus=$(($exitstatus+$?))
   done <<< "$squid_helpers_list"

   # Disable SELinux
   setenforce 0
   sed -i -r 's/SELINUX=enforcing/SELINUX=permissive/' /etc/selinux/config ; exitstatus=$(($exitstatus+$?))

   return $exitstatus
}

### System user add
function sys_add_user {
  user_name="$1"
  USER_HOME="/home/$user_name"
  USER_SUDOERS="/etc/sudoers.d/$user_name"
  SSH_DIR="$USER_HOME/.ssh"
  SSH_KEYS="$SSH_DIR/authorized_keys"

  echo "Checking if user $user_name exists."
  if id -u "$user_name" &>/dev/null ; then
    echo "User $user_name already exists"
    return 0
  fi

  useradd -m $user_name && \
  install --verbose --mode=700 --owner=$user_name -d $SSH_DIR && \
  install --verbose --mode=600 --owner=$user_name \
    $prog_dir/system-config/ssh-auth-keys \
    $SSH_KEYS
  echo "$user_name ALL=(ALL) NOPASSWD:ALL" > $USER_SUDOERS && \

  return $?
}

# Disable root SSH access
function ssh_disable_root {
   sed -i -r "s/^(PermitRootLogin\s+).+$/\1no/" /etc/ssh/sshd_config
   return $?
}


# Install INCRONd service
function install_incron {
  yum -y install epel-release   &&
  yum -y install incron         &&
  systemctl enable incrond

  return $?
}

# Initializa statistics sender
function install_stat {
  nstat -r -n &&
  install --verbose --mode=644 \
    $prog_dir/../system-config/logrotate.netstat \
    /etc/logrotate.d/netstat

  return $?
}

# Configure cron tasks
function configure_cron {
  systemctl stop crond
  install --mode=644 -t /etc/cron.d $prog_dir/cron.d/*

  return $?
}


# Remove temporary files after automatic install
function auto_install_cleanup {
   tmp_dir="$AUTO_INSTALL_DIR"
   if [ -d "$tmp_dir" ]; then
      rm -rf $tmp_dir
      return $?
   fi

   return 0
}


function detect_yum_problems {
  echo "Detecting YUM issues"
  yum makecache
  return $?
}

# Perform actions
step_crit "Base OS version" check_os_release
step_crit "Checking YUM functions"                detect_yum_problems
step_crit "Creating administrator user: $admin_user" sys_add_user $admin_user
step_crit "Installing admin tools"                install_admin_tools
step_crit "Installing INCRON service"             install_incron
step_crit "Global configuration initialization"  $prog_dir/modules/install_proxy_conf.sh \
  --action install --interface $srv_if --user $admin_user
step_crit "Current configuration backup" \
   backup_config $global_conf_dir/backup/aps-proxy-install "$bak_files_list"
step_crit "Configure SSH service"                 ssh_disable_root
step_norm "Network interface configuration"       net_config_sanity
step_crit "Setting up new host name: $srv_name"   configure_hostname $srv_name
step_crit "Setting time zone"                     set_timezone_centos7 $server_timezone
step_crit "Configuring DNS resolver"              configure_dns
step_crit "Adjusting global perfomance parameters" tweak_global_performance
step_crit "Installing ipset"                      install_ipset
step_crit "Installing firewall"                   $prog_dir/modules/install_iptables.sh --os_version centos
step_crit "Configuring firewall"                  configure_iptables
step_crit "Installing Squid service"              squid_install
step_crit "Installing Squid configuration files"  $prog_dir/modules/install_squid_conf.sh --action install
step_crit "SELinux configuration"                 configure_squid_selinux
step_crit "Configuring CRON jobs"                 configure_cron
step_norm "Setting random password for $admin_user" $prog_dir/modules/generate-user-password.sh $admin_user
if [ $? -eq 0 ]; then
  rnd_password=`tail $STEP_LOG | grep "New password is set" | awk '{print $5}'`
  echo -e "\n\033[1;34mNew password was set for user \033[1;36m$admin_user\033[1;34m: \033[31m$rnd_password\033[0m"
  echo -e "\033[1;34mPlease remember to write it down for future use.\033[0m"
fi
step_norm "Removing temporary files"              auto_install_cleanup

echo -e "\n\n----"
echo -e "\033[1;32mProxy initial configuration is successful.\033[0m"
echo -e "Server's primary IP address: \033[1m$srv_ip\033[0m"
echo "Please reboot your server to complete this procedure."
echo -e "\nWaiting 60 seconds until automatic reboot. \033[1;33mPress CTRL+C to cancel\033[0m."
sleep 60
reboot
