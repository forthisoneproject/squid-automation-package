#!/bin/sh
# This script was generated using Makeself 2.2.0

umask 077

CRCsum="293357216"
MD5="bdf16ca8a5d71213eb35bc261d61029a"
TMPROOT=${TMPDIR:=/tmp}

label="APS Proxy install"
script="./deploy.sh"
scriptargs=""
licensetxt=""
targetdir="deploy"
filesizes="24286"
keep="n"
quiet="n"

print_cmd_arg=""
if type printf > /dev/null; then
    print_cmd="printf"
elif test -x /usr/ucb/echo; then
    print_cmd="/usr/ucb/echo"
else
    print_cmd="echo"
fi

unset CDPATH

MS_Printf()
{
    $print_cmd $print_cmd_arg "$1"
}

MS_PrintLicense()
{
  if test x"$licensetxt" != x; then
    echo $licensetxt
    while true
    do
      MS_Printf "Please type y to accept, n otherwise: "
      read yn
      if test x"$yn" = xn; then
        keep=n
 	eval $finish; exit 1        
        break;    
      elif test x"$yn" = xy; then
        break;
      fi
    done
  fi
}

MS_diskspace()
{
	(
	if test -d /usr/xpg4/bin; then
		PATH=/usr/xpg4/bin:$PATH
	fi
	df -kP "$1" | tail -1 | awk '{ if ($4 ~ /%/) {print $3} else {print $4} }'
	)
}

MS_dd()
{
    blocks=`expr $3 / 1024`
    bytes=`expr $3 % 1024`
    dd if="$1" ibs=$2 skip=1 obs=1024 conv=sync 2> /dev/null | \
    { test $blocks -gt 0 && dd ibs=1024 obs=1024 count=$blocks ; \
      test $bytes  -gt 0 && dd ibs=1 obs=1024 count=$bytes ; } 2> /dev/null
}

MS_dd_Progress()
{
    if test "$noprogress" = "y"; then
        MS_dd $@
        return $?
    fi
    file="$1"
    offset=$2
    length=$3
    pos=0
    bsize=4194304
    while test $bsize -gt $length; do
        bsize=`expr $bsize / 4`
    done
    blocks=`expr $length / $bsize`
    bytes=`expr $length % $bsize`
    (
        dd bs=$offset count=0 skip=1 2>/dev/null
        pos=`expr $pos \+ $bsize`
        MS_Printf "     0%% " 1>&2
        if test $blocks -gt 0; then
            while test $pos -le $length; do
                dd bs=$bsize count=1 2>/dev/null
                pcent=`expr $length / 100`
                pcent=`expr $pos / $pcent`
                if test $pcent -lt 100; then
                    MS_Printf "\b\b\b\b\b\b\b" 1>&2
                    if test $pcent -lt 10; then
                        MS_Printf "    $pcent%% " 1>&2
                    else
                        MS_Printf "   $pcent%% " 1>&2
                    fi
                fi
                pos=`expr $pos \+ $bsize`
            done
        fi
        if test $bytes -gt 0; then
            dd bs=$bytes count=1 2>/dev/null
        fi
        MS_Printf "\b\b\b\b\b\b\b" 1>&2
        MS_Printf " 100%%  " 1>&2
    ) < "$file"
}

MS_Help()
{
    cat << EOH >&2
Makeself version 2.2.0
 1) Getting help or info about $0 :
  $0 --help   Print this message
  $0 --info   Print embedded info : title, default target directory, embedded script ...
  $0 --lsm    Print embedded lsm entry (or no LSM)
  $0 --list   Print the list of files in the archive
  $0 --check  Checks integrity of the archive
 
 2) Running $0 :
  $0 [options] [--] [additional arguments to embedded script]
  with following options (in that order)
  --confirm             Ask before running embedded script
  --quiet		Do not print anything except error messages
  --noexec              Do not run embedded script
  --keep                Do not erase target directory after running
			the embedded script
  --noprogress          Do not show the progress during the decompression
  --nox11               Do not spawn an xterm
  --nochown             Do not give the extracted files to the current user
  --target dir          Extract directly to a target directory
                        directory path can be either absolute or relative
  --tar arg1 [arg2 ...] Access the contents of the archive through the tar command
  --                    Following arguments will be passed to the embedded script
EOH
}

MS_Check()
{
    OLD_PATH="$PATH"
    PATH=${GUESS_MD5_PATH:-"$OLD_PATH:/bin:/usr/bin:/sbin:/usr/local/ssl/bin:/usr/local/bin:/opt/openssl/bin"}
	MD5_ARG=""
    MD5_PATH=`exec <&- 2>&-; which md5sum || type md5sum`
    test -x "$MD5_PATH" || MD5_PATH=`exec <&- 2>&-; which md5 || type md5`
	test -x "$MD5_PATH" || MD5_PATH=`exec <&- 2>&-; which digest || type digest`
    PATH="$OLD_PATH"

    if test "$quiet" = "n";then
    	MS_Printf "Verifying archive integrity..."
    fi
    offset=`head -n 498 "$1" | wc -c | tr -d " "`
    verb=$2
    i=1
    for s in $filesizes
    do
		crc=`echo $CRCsum | cut -d" " -f$i`
		if test -x "$MD5_PATH"; then
			if test `basename $MD5_PATH` = digest; then
				MD5_ARG="-a md5"
			fi
			md5=`echo $MD5 | cut -d" " -f$i`
			if test $md5 = "00000000000000000000000000000000"; then
				test x$verb = xy && echo " $1 does not contain an embedded MD5 checksum." >&2
			else
				md5sum=`MS_dd "$1" $offset $s | eval "$MD5_PATH $MD5_ARG" | cut -b-32`;
				if test "$md5sum" != "$md5"; then
					echo "Error in MD5 checksums: $md5sum is different from $md5" >&2
					exit 2
				else
					test x$verb = xy && MS_Printf " MD5 checksums are OK." >&2
				fi
				crc="0000000000"; verb=n
			fi
		fi
		if test $crc = "0000000000"; then
			test x$verb = xy && echo " $1 does not contain a CRC checksum." >&2
		else
			sum1=`MS_dd "$1" $offset $s | CMD_ENV=xpg4 cksum | awk '{print $1}'`
			if test "$sum1" = "$crc"; then
				test x$verb = xy && MS_Printf " CRC checksums are OK." >&2
			else
				echo "Error in checksums: $sum1 is different from $crc" >&2
				exit 2;
			fi
		fi
		i=`expr $i + 1`
		offset=`expr $offset + $s`
    done
    if test "$quiet" = "n";then
    	echo " All good."
    fi
}

UnTAR()
{
    if test "$quiet" = "n"; then
    	tar $1vf - 2>&1 || { echo Extraction failed. > /dev/tty; kill -15 $$; }
    else

    	tar $1f - 2>&1 || { echo Extraction failed. > /dev/tty; kill -15 $$; }
    fi
}

finish=true
xterm_loop=
noprogress=n
nox11=n
copy=none
ownership=y
verbose=n

initargs="$@"

while true
do
    case "$1" in
    -h | --help)
	MS_Help
	exit 0
	;;
    -q | --quiet)
	quiet=y
	noprogress=y
	shift
	;;
    --info)
	echo Identification: "$label"
	echo Target directory: "$targetdir"
	echo Uncompressed size: 672 KB
	echo Compression: gzip
	echo Date of packaging: Mon Apr 11 13:26:50 EEST 2016
	echo Built with Makeself version 2.2.0 on 
	echo Build command was: "/usr/bin/makeself \\
    \"deploy\" \\
    \"/tmp/aps-proxy-install.sh\" \\
    \"APS Proxy install\" \\
    \"./deploy.sh\""
	if test x$script != x; then
	    echo Script run after extraction:
	    echo "    " $script $scriptargs
	fi
	if test x"" = xcopy; then
		echo "Archive will copy itself to a temporary location"
	fi
	if test x"n" = xy; then
	    echo "directory $targetdir is permanent"
	else
	    echo "$targetdir will be removed after extraction"
	fi
	exit 0
	;;
    --dumpconf)
	echo LABEL=\"$label\"
	echo SCRIPT=\"$script\"
	echo SCRIPTARGS=\"$scriptargs\"
	echo archdirname=\"deploy\"
	echo KEEP=n
	echo COMPRESS=gzip
	echo filesizes=\"$filesizes\"
	echo CRCsum=\"$CRCsum\"
	echo MD5sum=\"$MD5\"
	echo OLDUSIZE=672
	echo OLDSKIP=499
	exit 0
	;;
    --lsm)
cat << EOLSM
No LSM.
EOLSM
	exit 0
	;;
    --list)
	echo Target directory: $targetdir
	offset=`head -n 498 "$0" | wc -c | tr -d " "`
	for s in $filesizes
	do
	    MS_dd "$0" $offset $s | eval "gzip -cd" | UnTAR t
	    offset=`expr $offset + $s`
	done
	exit 0
	;;
	--tar)
	offset=`head -n 498 "$0" | wc -c | tr -d " "`
	arg1="$2"
    if ! shift 2; then MS_Help; exit 1; fi
	for s in $filesizes
	do
	    MS_dd "$0" $offset $s | eval "gzip -cd" | tar "$arg1" - $*
	    offset=`expr $offset + $s`
	done
	exit 0
	;;
    --check)
	MS_Check "$0" y
	exit 0
	;;
    --confirm)
	verbose=y
	shift
	;;
	--noexec)
	script=""
	shift
	;;
    --keep)
	keep=y
	shift
	;;
    --target)
	keep=y
	targetdir=${2:-.}
    if ! shift 2; then MS_Help; exit 1; fi
	;;
    --noprogress)
	noprogress=y
	shift
	;;
    --nox11)
	nox11=y
	shift
	;;
    --nochown)
	ownership=n
	shift
	;;
    --xwin)
	finish="echo Press Return to close this window...; read junk"
	xterm_loop=1
	shift
	;;
    --phase2)
	copy=phase2
	shift
	;;
    --)
	shift
	break ;;
    -*)
	echo Unrecognized flag : "$1" >&2
	MS_Help
	exit 1
	;;
    *)
	break ;;
    esac
done

if test "$quiet" = "y" -a "$verbose" = "y";then
	echo Cannot be verbose and quiet at the same time. >&2
	exit 1
fi

MS_PrintLicense

case "$copy" in
copy)
    tmpdir=$TMPROOT/makeself.$RANDOM.`date +"%y%m%d%H%M%S"`.$$
    mkdir "$tmpdir" || {
	echo "Could not create temporary directory $tmpdir" >&2
	exit 1
    }
    SCRIPT_COPY="$tmpdir/makeself"
    echo "Copying to a temporary location..." >&2
    cp "$0" "$SCRIPT_COPY"
    chmod +x "$SCRIPT_COPY"
    cd "$TMPROOT"
    exec "$SCRIPT_COPY" --phase2 -- $initargs
    ;;
phase2)
    finish="$finish ; rm -rf `dirname $0`"
    ;;
esac

if test "$nox11" = "n"; then
    if tty -s; then                 # Do we have a terminal?
	:
    else
        if test x"$DISPLAY" != x -a x"$xterm_loop" = x; then  # No, but do we have X?
            if xset q > /dev/null 2>&1; then # Check for valid DISPLAY variable
                GUESS_XTERMS="xterm rxvt dtterm eterm Eterm kvt konsole aterm"
                for a in $GUESS_XTERMS; do
                    if type $a >/dev/null 2>&1; then
                        XTERM=$a
                        break
                    fi
                done
                chmod a+x $0 || echo Please add execution rights on $0
                if test `echo "$0" | cut -c1` = "/"; then # Spawn a terminal!
                    exec $XTERM -title "$label" -e "$0" --xwin "$initargs"
                else
                    exec $XTERM -title "$label" -e "./$0" --xwin "$initargs"
                fi
            fi
        fi
    fi
fi

if test "$targetdir" = "."; then
    tmpdir="."
else
    if test "$keep" = y; then
	if test "$quiet" = "n";then
	    echo "Creating directory $targetdir" >&2
	fi
	tmpdir="$targetdir"
	dashp="-p"
    else
	tmpdir="$TMPROOT/selfgz$$$RANDOM"
	dashp=""
    fi
    mkdir $dashp $tmpdir || {
	echo 'Cannot create target directory' $tmpdir >&2
	echo 'You should try option --target dir' >&2
	eval $finish
	exit 1
    }
fi

location="`pwd`"
if test x$SETUP_NOCHECK != x1; then
    MS_Check "$0"
fi
offset=`head -n 498 "$0" | wc -c | tr -d " "`

if test x"$verbose" = xy; then
	MS_Printf "About to extract 672 KB in $tmpdir ... Proceed ? [Y/n] "
	read yn
	if test x"$yn" = xn; then
		eval $finish; exit 1
	fi
fi

if test "$quiet" = "n";then
	MS_Printf "Uncompressing $label"
fi
res=3
if test "$keep" = n; then
    trap 'echo Signal caught, cleaning up >&2; cd $TMPROOT; /bin/rm -rf $tmpdir; eval $finish; exit 15' 1 2 3 15
fi

leftspace=`MS_diskspace $tmpdir`
if test -n "$leftspace"; then
    if test "$leftspace" -lt 672; then
        echo
        echo "Not enough space left in "`dirname $tmpdir`" ($leftspace KB) to decompress $0 (672 KB)" >&2
        if test "$keep" = n; then
            echo "Consider setting TMPDIR to a directory with more free space."
        fi
        eval $finish; exit 1
    fi
fi

for s in $filesizes
do
    if MS_dd_Progress "$0" $offset $s | eval "gzip -cd" | ( cd "$tmpdir"; UnTAR x ) 1>/dev/null; then
		if test x"$ownership" = xy; then
			(PATH=/usr/xpg4/bin:$PATH; cd "$tmpdir"; chown -R `id -u` .;  chgrp -R `id -g` .)
		fi
    else
		echo >&2
		echo "Unable to decompress $0" >&2
		eval $finish; exit 1
    fi
    offset=`expr $offset + $s`
done
if test "$quiet" = "n";then
	echo
fi

cd "$tmpdir"
res=0
if test x"$script" != x; then
    if test x"$verbose" = xy; then
		MS_Printf "OK to execute: $script $scriptargs $* ? [Y/n] "
		read yn
		if test x"$yn" = x -o x"$yn" = xy -o x"$yn" = xY; then
			eval $script $scriptargs $*; res=$?;
		fi
    else
		eval $script $scriptargs $*; res=$?
    fi
    if test $res -ne 0; then
		test x"$verbose" = xy && echo "The program '$script' returned an error code ($res)" >&2
    fi
fi
if test "$keep" = n; then
    cd $TMPROOT
    /bin/rm -rf $tmpdir
fi
eval $finish; exit $res
� s�<\��kw�6�8����gϏ�w_���؉���č�Ǳ��Mb��ݤ��(�DY�%�!)_r��������  ��,;q�%{�0 `0�K�R��|�)��fO�Zm-.2����V�M�����j}��j-VY��Xl�~`�����CSƎ5v�hZ>�6��IxX��������������/����L<�������������w1����;�'������T����{�lz��*����鄶c��R��o��ثݯ���;����kK�K�������j4[��ƃj�n4W[�ͥ�z���s�\��_�����%E����l�����r����3����C�j��ټ���Ԭ����X��������Q�l���2�_k-�����$�+#�g�*�����o��D����ݭԪ���wݐU��bz�As�Bʘx}3����?*C����p�,���Y�o!a�7���!���������{�����)����(����ߺ[�����'v?�YG��#��*8��s���B�����Um�_��������S�l�o6�����R��������b����3�>����Lbp�7�[�%XP���]��
p�Iq8i���[N�|Ա�\�ς�xl�g,��,tY���!�a�Ȓ4���Ѹt����Ûd��`5Қ����FA���_��׼��M������V+�?�N���6���$ ����7�iV��b ���KA؞9	�_�;�! �py�`�9�{���߱Bd����/��'����������u���?����V��q�������R����|��+Ŝ2�ph��M˰x�2��􋋂oJ�}��g��oV�����F��`�F�������Z��ϭ��+&��v�u���َ�$V'$k�n�bk�3�'�ڮÎaW6FV �z�`l8�$d�H���(!4�Vh��5��(�t4�`^l=e{di���v�V�
���`�<y�����p�#V�%����4��=�9P�kK����=��'������'f��;=����֩~��!m�6�7�5X��A�݆F�_	΂!�)��z��A���F�ק�z�Y�n>Q+�p�G��+���eC���-��l��?k���c��X+��`����iH�kn����eV�-z��S70D���
Bh��.�Y+��WBBc��X}8�Ñ{`��ݾ�C��_����v��Њ����¢�6
Q���u-N� ��S)�̳��3��m�t�L�+��r�o�����C�z*"5h�!��@=�b��[���|��l��>�<�6�� �d����O c�.$�z�k����~d����,��7?3�H�8Q������Yf=ӹ2�&|�����8.��`���6��r��i�x�.ҝ��G�=��)��\�4���c8�4Y��T��9�b�FbX<KK5l�����c�tV�� �k���/�g1�a�Xۉ�;А�ze^�4���	���W�����І�����p����Xh�EWl: յ����앋��y��X�ĆB��Y����B=����L�T�47aK�a��d�v]��-[]9l_�@�ף�/A���9
�}���w5B$e룖����YCw%�����V��� k�/@�aY�� m;s'>sO���Q�����ʴi!�kU�<\���r�Ƌ��h�=�
�������1\?,���#�� \���
�=1�L�#�=�|��9#������G�x��{7>�T�M-�*	�v�4��h�����rQ�]c�	nz��	�:Qo^�Q�)�ȡY}�����C{�B��O?� �����L؝�y�����%�� �ƃp��(�����ȊpY�Zf�j
y�50B�\#$�������D3���v��_`�t��.�3tyƮȑ�/8>1ǡ��ۍ��G��?\���^H�"�]D�n��cxm�PS[ dr `��ۆdg�Xd�c8���K=�K���v�w�v�t��
���'�]N��?�N�~>�a7ݞ;�+P����=�Rx��X�mN+�ul['|=����4��	�ɍ�(��hbъCC#Jb�Dp��͗ݵ��ݝ�/��:r�17�;��+� �{�(="�P��H(�ӧx��P��iO �阇�n>�c�qř��^�SMEz@��bH�M�
B��ɒ����y���%P�,���ʜH0�E2Qb�����Ud'a�paaU�ָ��Q�+:� ��d2�}�׳'�٢�w�������K="��y߁?�,'��
�>�}��ρs�ǎ�����|��3����;��1L��>�м�ٙ=�}1�q�x��9����1�������S�C����V�^����)U�_M�_��ҟ�妆�����(�ȡ�� ��'��.����5#�9Ej;paM1���~r��>'C$�H�x�y��	���ZU���Nʖ�&��������Nց��u�nh��Ҙ(�#�:�F)|]��^苳��|�K�L�k��!�+���K��C�<�*�F`���;�%R}˝\��`����2>i���Q�97ʴ�<�ǜ ��7��{�\
��D��`��/��)ome�����i� ���D^���{�� \7�>q~�X#�O�
pl�I 7-]B㟣�LB�Y#7��0G"����:o�r�&Z�lم�B:x8�_t�ȜK��3N3�3ٞG�]���$wS؂�Z��@�&��t:�U��XpR���cA�x��qF�bo`��V'�A�W}��'��1m�c8�2���$��*�����ۙg��������±������<f�o@�R	p`*O29�+J5��\�^���En�$�f��
���]�N%�EB�W���å���xB�NCX��>Ȍ��dصN�^VV�X
����x�Mf�Ym{in���+�2?O��">d�� ���b|��&ȉ7�.R��wh��ǌ;AEP�
��}�c�rG,���*�D0��*��iѺ�O�(�c�DeQ=�� �p1���u�;�g[/�+ze莭J)�%w�ַ�;)(��]@l�ɬ�γ��� �`V�
�x��� �$�UPQ���V�{d��t>a�����S�׃h
�Ra97&0�q;�O
������f�pB�	�|j*��I����Eᘵ�ҪV�'о�(Ō>��	b)��'�Z�*�J�G(V���x�2�̳ͭ��N���2���I|�ڬQ	i�@=�h�L�`���B�.(	G*���F	w ��{h;�����Re��]PL0���ⴟ�C�ycsmgk�߻�=+��q�>���<kɈ_l�/,ʒ��(S?a���`�Z�x3'�LKI����/��ᤘ�����rj���C8ٛ�Uw�*�'���qNGbփ��AB�q�S�Z� �#Q��Nre
�B��n�9�Z��10��sIJ�/L�A��bt��1I����3��$Ǟ��X����nlvv�4�v�h���K"cF	��0�|Q	X�}����d0����!~!��{/��
&-a8]���3{���6?f�ҝ�c���!�㤏�}zF�f�Vi�=�"��g�>$��L} 90f�Ok�2+ŷXzr+SRThb��q�'Ӧ��(|%������
@�|����}H�eٛ*	�8UZ����x�ĺ 쌸�0�����H�� xЉ�s��/b�e��%c�'g ��[z�����C.&�Lm��3U��C':!�eD�j3�8h%�x�[�e�>�<�<�X��.�����s�9Wx��5s�6J� ��IH���w��Zv��_�P%�nէJ^���󻘔(L)?�}�~灘NR��T �PЕ����vNeY�͔Ǌ�ٚ�T�[>{��K��$�4�'�8��B^2�i����{�)^�8uJR�M��a�P+��M���~Z�ۢ�`Ȓ�wu�N�\m�����2+oC8���+�te�=j�R�<N����HR�!d�q���I �k���|Kc��q�e&o�Kjc�{֤Ё
E(ck| u�.;���2�s�C�IT��/��C�D%����q?�����Z����6IT�ZSj�LM�(0���6Ƣ�H�)R£+�L�Ҹ�O���� Ot�$$�t�=�F��	�����> ���ߑ��K�M��	a��\"�^V.ͷS��=\.G�m �ߖ/U5^�б�w���bq�!�Oa�A���f��D���B����(�?���%k�Í�?��M�c��T�����/wU�U�\��{��(��b�/��k��oE�3��jf����������PZ@> X'��I�u�J�{��b1��'�H96�J��HtP�D��O��;�O\Ҵ�O���:�\���2#�m!a)������b�eR	K*2"�}G��<j�2���[Jo���Q��'�g�l�9��']���Y0t�_Wt{9\�;����O��e=V6>0��xT;��������Q����
��W�Qu:3P�/���
2�H��>�M�F9�f똛�q8���p��P��I%?�)ۉm�$�組dKL�R]O|�� �������*��l�ė�����_�$歜)\�<�=��~Yϔ��V��R_�Ҏ�v�n���F�	���2+�"�mn�A�4�!��z,8sB�4��E�[�ٓ�58��g�z���P�
 �R�2K��]������r�B��[d�<����/|�xD�%*�U"�֊��,���h1���c��=O�\�NcIl 1���!	o�.�W4C�D��Lm�H�D��\&2e.��J���+Z*�kjBD�^	h���g�H"����d봜�phr��O�Թ��_��(����w#�+��Y�_�6��C*�C�8�}翬Z��������V�Jjf��MQ�����ۼ� �'�9��ܔ�ȝ�- ʟ�2@�[�a�k��N�r_���[��z7�{��	&���	��7>�/� �|�z�ʳ�z�M�b�e�����<�F�w`�vaz�0
��[�}�M�cOh5]�F;�P��M�Rt�F�!��Ѽ�Ng��J�
����q����r�����`��Gv�$��&l��R,�+�Ff���^�>������V�O4n�����dy�hhF�5�Ht��Ԋ�a;n�E�����;]��Y��nx�����%� ����.9�){�Nzü��>H����nz�D}�=��Ȅ�\#��^m�y�1=���ƺ­SPOC��׻�'/�X�{;� B+�x��m9aP�*]��l�
��4Y��]���\�SG����aJ�s;��h���v�|dȼ���}
,�c���1R��6�g�����3K�Q��A�?>���J�6�u���.~Y��ۣ�o9��<]i(�´ue��f�Z��۝�5v�����;�pQ!�K�pC8R�L�����h$���W���ЅL�`LeO3Y�az]n1�g�t(� �)]���\�a��4���vEhQ4X9V����lu�Cƨ��$:��z��c�9
`�Yщ�;�gn�ڥ��ӆ�p���݊2&���5gX�̑��� y�������z<�_l=�ؼ�9<eϜ���E3�m`$���bMaJ����̃<�L(���~�~r� #}�2��i���F)������B�t�O�j�n�ߋ) �j�&`�p��	�^$��ܞ6�$N�PM�©.�,�H��k_�,'�
�Pn�h����ԯKP���������-��.��}��(�?��B������&�/V�4��jE�������12�i	.1����شG$���-��軴�v�A��h������Êp�s�H�&��!)x�`�.�9�B4�&Y	�qie�DP8\�(5tF�Р+��HP��]rWd_;�r����ǖ	w; �<�+�	����2�D@".��eo�i��Zߊ�w��Ԍ����<Ix�jR6��_T����B�V���5��V�*��B���	��v=�����t2�F��Y�1���%������u�#�|�b:{���N�Q�DQ�oO�0=m��t7W_�W�*���u��N>t��W*�?���ߍ��J�y��]�W@4�������Y>8{6:�	�-�҉��}��A�����F�Gh����"ፓ�~�\��唷��5�%�N0�t���\� ���4('0FeE���~��atQT	]<X0�%�S �����˗���l{uн���HN��Nx<I�>�yů���е�f�A�B)˨�SO�j�0��n��lmL�&l"��Z�K�$���s��O1�R��+��o/UV.����p��BC���AEZ�{�-����J�S��D$���3��J�A#!���)�����)��r����n=W�0纙���iz���e�HGt�9j��&�2b5�gRjEI,?�;��R�#PcL�z3��P��2�����g�[��[���8}
�R|R	��X��%M֣^����?r�m�wNG�Y�e;B������c:�GK�����J
���S鋍�nw�I��ދ]�k���L�0���k��墇{���=��7n?��R�݋�U�V.E��1�2]\L��ؿx�ʫ%����Ll~�\V%W���(~�e���v+�j�R��'h3`Pac�U��cGV >�(F�qdq$���)>L�����X���?[��B�s��?R1��f�0Ͽ�򾱵�r玦�]����p�|���5Z��-Q��o K�]Cٍ��D>���%w+�"������t>M&N8���}0� ��~�!�\{��J��J#�Eg���k��X���Z8_�����I��|���)��)z8�	�(-W�&�s�ѷ1��8"�D<��<4��	1�>�ؗŔ�WtT�Y>��Q�pb�PV�og�p:�wr8Z/��X����.��\]�H	'V��L��[xTW�c=\O���vn>�0�K~��\l�4 m��D>C��_�ܗn�����zۏJi9~��D�A�76_��*���1�;F|���w�^�;�kD��_[,{x8�j���{�l}gkΑ��7�򓭝W�;�o[{��mum���+>��6�t:A��w�/Vw���z9��<k�3�(����$Sʄ=/[�f�~�4�Oנ�� ��]�P�c-�d�H#:�!}�2��C|��̪�z��ZqI�y����=1�DV����Z�(7����G��B"���ʤ�4U�#/�2���u'C� ��|ja������w��]Ɂ�� ������S�4m��S�=ҵhu�p�����^�.�|���\b���-�;���.x����Q��Y�奮��皋�%�yސ�W���)����8�_5�϶��5ki��՛��_R��L��.S�i�`o�j����a6um�Q�v~�q�(�Æ2/M��!�DlfsV8��+%�y������ϽO�}}���X沽<�b���ky�\�����OWj�e؟�qN����x.I�9�c����*Du�H��*�Ⱦ3��ZLq?��f�c�L�a�k�;ͪ���t�1�1����ʐ��=ë<��/#�RO�~J�c5�#�w�u�ē4����o���6�+�͟o--��33�����<s\��`�]��
��,�fC�=���-x���Q��e�<ͤ'�P-73�W�X�S3�Lhfe�mq"3� �ʡoZ~ڳ}Nf���S�����5�^W�8����vn��lf��c��#��#�,�3�h?2��M��8�L-rT�Ւ��_^���Sk�_RKb=a,ӗD�L{̄E��h^D{"����d$�^�"�@�s�I��O�L_2��	{�R|Y8�J��S,W�ְɞ]ȒYZ/%y"4�LB�#P�K���l�*��
�Q�ap�#،Vʂ��Q��9�Ds��{�̏!x��p*:���Mϋ�ƴS�4��-L�H���(��^�^ʮ
�B�UU�\^��'��ۀ\����R�����B������&�����4�7�
��������,�寒�k�؋\쯥���~��o�nX����b=1(U���eZ�Z��4�.�#�!�̉�Y��7�����g8%Z5X�,�ӯ�.��Q�.-�������������_������bA�7��mn�1>�"<;�¾R�C�F���#�;�G+:�N
�+����2O+��aeg���t������eq��ų��OC�gK��.�����2��5޿�܇y(����
Ĕ����#�'�K��_��x�*�W�|��%IO�I�9`���+oI5z���-M)���|����ı�~ ��4��l�\����#Y�(����wG#��^ϖ��2��
�줠a����o�r�":p�Ђ�HT�
�!�1lVJ�K�7*����	��vzv�铧����ˌ�ț�g^c�V/v��9.�1u�:�q��&r<5�e���C��Bp(e���R�H�G���S�ޢF�uӸ��S�ެ�D\?wK1��ǰǤ�_J.J�j��P���a�.V���5���P�Z����.���£������V��)}䜢3G}vA�8u���`>��W�2�o�U*��bN����1�T�P�W���ה��`P��R��e̴�a��7��J�Z:Z��:�.vu1�I]ahY�6�e"N����Z}�&l�"]4a������r�4rH�Z��������B��8��k��oE�3�����__,�\��/b��纅���|��v��uh&y�˻1KF[D�F΀�����k���Ni�����?{����U���g[�W��ت��t]�v�ַ�;ip �!���d�N�Yw}c�:�J9�C%�1 �D.�������<%j$�9&����4�9���E����uX[�j6�. �t��#�un�D�oIև)�l�I>�K��5���&[��qc�V;������o��o�Uwb=w�Y���w���O��f������ޅ��_�y�~�lk���M���џ�=�����_������~=XZ�.�p�������֩�[���������z��2_��-U7W;�ׯ��gX�������������Ns�t�uN������_�����mw�z�����������v�0��^;jm=Y�o�o�O�h����W�l�;�qO��js2�힘�_��wN{��������?��׵�Ok�w'Oַ�ΰzp��~������^���7v��^n�*/ܧ��ǻ��r�`��?�>��|=i�����Wg��a��ރ�϶�L��m�k�������h����G����f�wZ��{��q����+s��<���^�t^�/Û�#�Ny���H�����?�����Ws_r��(��_���H�ߊ�g���	 s�S+����y*� 3b���Aлdd���j���嫉�	2����M�ƛik��}��=~�v|� �,�=X!�6�Jw�R��A�Rc=w����8��4��we��ET�F����o5�&?k�p��uP�R�[�c�?��\RkE��ސ���E�O�Z�Ժl�̒�4�pU �ġ��3�p�U|+�m�/w���W_����V���������
��:�?I������^R�_��?��_���(��&�F\�����o���������S�l�_�.��Y����HfB�^p:�Ah�0l��$�&!㾵� '��V����pa�k���V/�9�� ;�4#(�RA��t�tM1Ĺw;x�����uzBf|x�ᷮ���Gn�E���JT�a������ƃ7�����Re����;	/-��Tp\P��xC�_����DѾ'�/sp#r�W_����V��������S�L�������֪����{8�(4�7N��Fw���I!�B�3�!�4�O�l+T�c�s�U���1PGh!�n`�������e�yr��|�qKJ��wXIi�4R0�L���2�������&����M�=j{Duh�f{�r0�``�D�D84C��Ǆ�H^��E�j��Q��0QݘD�Ų��]��]tFA����CWa�wI��Vm��W��O�WN�������o��q����EZ4�<���6΍�l�yi4��(�J�|�vhu�.�[����b�\-7����{h;+��+���M�gR���b�J��>��� ��P�"���#�5������f���3��U|z�Ћ���0��MZ*?���5�&3�d%��;a1J�o<E���H���A.)�������:�\���y���ho��oq�U���W��N�WN���VmT���_�_?�� �8�~���6��s��|��k	i%�6��X=�n����%�o�]�(�Z���"D᠀��wp |�=���H��H�3�«�2Bq����dv�&�APJE~m6s���֋�xA�GS�o+ls���ë�eTN�-a���x��wWw�O{s������9J40�-��L�8d��h�;��!ޭ��i�(d�����r��5��<`N� �/Rk(VN� ����XS��!F
�sn��c�`�P�̑�Aį�&ݺ˝��a����TIs�o�F�o1�d;]�`��>�Z��xRܡ���(�pܕ9���~���7�u�)R��L��~�,��|��W�!2����S�(5���0���ɹ"��i���?�?��K���O��.�/�|9���'�&>Ε���-=ۯ���oV�>6^H68��&a�Om����e�=8O�,�e�ˇBQ�{�Q��;�6�O'�����f���sI)^R˗b ����?|��g�`/���≧>\�1�m\�\�� ��u�`'*T�(%���@M苕�h���_��kk�H�9՛����V=*y6���5����ǫϻ�6��"�Y�B�p��o5Y�l���E�1(;o��|BO�gQ8d�L�׵l� 9��S� �i�/�t�*�,�Y�����R���i���w�^fn+~�<e����` e�)@/|�,�b;�$��7-�&��{�RZ�(!�e���������]��#�������\zK��H����bk��|�[�5��G�j�;�k��k�Wt�յDuw�$������\T<^��i�����؀W�|����r��@Ҹk�Ho�=+ɲ�������^�������B�U�K��/�����y�md㰩%Q@O��0{#V������X��T�d���Y�.���z���e!�Ph��e:��ͅ��R�^L�2cl��-/�fRď��w���q�h@0 X�@���nl��_�T��H�u�0�>��9��o>k��;"_e�	�(]��n��>�(J����{�W9<?N�lnd%��<+M협�ӡ�b�ͣ|Z	���8>�X���+)H��<@ߙ8)�خ?�-��4]��ZXI��|A�wK�F"�n+���="�:��-1sb��A�(9���������(��S1m$���ܝ���0��މc!��O;UܥS����Eb�>8�b[�}h;|���H"LdȩE�\3�f���i�;�{��$��aA��K%&ٌCUY-q����
���b��Xr$�:�9 ye�C�ߓ�|5�`�J�'�c\?���=�f�H!o���-��Q� �ȴbd�B�w����'�{u\Z�_k����_!�/���O�WN�3�^������(��o@��� ��I��wO܎��G|R�؂c�"���p8��0Έ��T^=� ܔ�#�P�	o�:I����B�!���F'Qt��pT#Π�*�u�b�Z�k=�J��1f�{ԛ�����/�rz.*�,S�uflŞ@j��'$$�8����"����A��ԥ��$��'%Q�T��8~8q�FO��(~�΅?�=<?3:�Q�5H�Ey�S���r�ɸ�Go�>� ��F�>#Lᑺ��g�Hn�ջ�Q�#��|�O�7�I��o�,���Z.���G�����o�Xx\:�p91u���p�gw��;Yn1���X�G��G]J78�Z50S2dj����S1��U��X���Ȉ�_���ߍB������f�������߭�R��k͂�oD�GF��C�ST�FM�Kӿ*Dߚ�->[�FD���R����5-��~�Gn&6)��)�� yM�TV�� b��;��9����!F��m�����:����|����e�Qd[�A��:������+U���C(��P�(Y4yK ��l��IW��,��B��o�I���l�
�Q
PtҘ�qz4��^�iaz�4m'R��z��b��h=4������'V�F��$S���ؑd)1c�@�P�U@��Ғ>*�����_7�W�.�������ʩ6�/��Y������/�u3����DeD]�[_�h�ʴ�������<��2�9P�X[6f�r �/ۅ��褴J���~���1K*2Jٶnc�I���P��R��$dF�N���$|U�}��_��7a�_k�?��_���O�WN�3�q�����*����������02�1Gg,�p:013�W��������+����#oB"I+�7�����?��Ƌ8�D�Kgwuw�SY�Zo�y��u�z~��?x�M��B�FKU]�Y��j���'��d���}�����X�����~;ʷ��p�n?2�������Hy�,`��t��3q��\$@� �N������lE��D��m�������HE�LwhAO?F�<ʵ���x��a)����N�=�w��p�#!�2����ddE�B�Ak�^��gd���8Y�*�bd�B���F�ٜ%;T�G@8��l�k��x5M�>�P�<28t�����EmV��UDׅ��K�0��mE����>�:���������o�2�$��a �1���GAԙ1������I� ����eh��:���H��xp�V�0��&G�6��ÕBbI$�C�ӕ�-o��S<��~����\lɟ`hBV���Y�r>�c�Jh�\T���$ IL�b�'3Eo��X��)S����|��?�e�^�P8e=Q4��Qt7���
_���$� 	(�����t<��ĉ��(Q~;9��+�^��h��zJCI�DL<��O�$bp�N�> /S�v���
}���w�)��{�9r0اK+,T[+�푅J��ٙ;��{7`%8sB�T�d�!�:+0{ZlݒX�p����-���v������9&�2�g���G��_-/;Z����p��wˇt�?/|����gv�Ҁ	� �!��M{�Qo2BVfl;�ɉc���k��.l� �0�Q78鈭3�h��$�����䝄�w�C�cU��Ç���w;k;ۻ�~���`��v�Zz�VV�n�|'��1&-[���x?8��/���ôֽ��}�W"'�pt[�/5��[�!~�օ���92��j��tTܙ��U�@yaN`v	|>2\��R�B`y�Bg�q��-	 Hs?�U{�}��c����}�D�(���~�od'�,�a�Ϛ�Z�8��:��Y�E>ƱD�JINd���҈U�ǰ�>��|��nF��}��]���\�1�G����Z[*������\;�_9�Ϥ�f����.U����ٱ�p-� 
y' s̄&�5� Mq��:ʡ��������N��A��Z�2f�����~����qYћ��A�Yo�����Zfs��x����L�`IWݖ��V���������� L�7�����Sy�b3��9	�d�8��wB�/���7�H��M'.l2Q���A�����b"�#_n/A&�m�L!T�?�v�a�&1��=q����j��C%-�a���1����{J�r��� ��Es��_f��@ŹSL��u"{1�/vQH)�ϣ<�BH�S�[��2�7%c�q�$�����
y��)4�k�씜ZL�K���	 ���t��^o+�O���RȒ��G�r��Ԋ��H���X�r~�����Y��'٪RzU'_�h�'���,yJ��K�͐������xJ��E؇�_��e���{�qdY�9b��3�����B��8������j�@��Ŵ�O�^��nP��>�:�)��<�~P���:�7�,۱���t�,���� ����1jD��L��.���\g��I���x(����$|�?ql�j<�XĹ�͖dV�G��4��-�z�CG3d�,�L��7�P��� ������n��ǌ�[�R"�(|w�(oh�_�y\�Ъ&�_��9�6�m.y�%p�|ޛ9�<"p���B8��=�S�ϗ�N��\�ߊ�X-�ۿ���I�E��J�/���7���_��w����?���b���Z/�o��G\���tgO�����[5�I[��h�OEv�a,��E�.PH\m�Rdnu�b"�laG��v����8Z��эM��-�81F5)SvD��ّ΍�0g�Rssge����HM���J�7?�K��X�:jR��y�Qh>aPa�H��G���ՠ��v%���=�g��u�ԗj��>T�\gt&п��9���l���aYr`nf��rF����r�iWf�=��̀�������
����/����^5�϶��72�ߖZ����WQ�)��)�/�����6M��m�)����OƸMǂ�i+�9|� ��;��Vd���U��u�0��
8@ed��&p1 "aK�sG#d�Ĥ P��E�a�������9e�|>�����Dq�t"%����b����߅��qC��ob��b���Z]� P�e�N��)Ɓ��|��Ăv1 񜉁�W�vG��,Zp��(�$`�M�0Tj���E�J��1	<ߤ.�A��&�/i� yɆ��l�Dh{2��*��E�1̢�Ժ��;]S�[W�)6�(JC-%?��n�\u��šr� Y�n_��͗X��E����}B�"�Y��t_�Ϋ$MK���S�|�+�2?O׭��>��<ە����X+"������lvC��:͏+:~��o���)�?������ʩ�"����ߥ����:���{��0C��m���8O4t��K]�/`�ㄳ��sݑ��x�!6%��(<��	��s�F&t�ٮ�_�z[�s��6A�w;x��dM[��Z�IK�bYBp�:�@N-%�I[J�H$��2��e#�IB�2��H�œ,8��b.�~a0��0a�Q϶���J�2c*%9�d*�<�h� ��3�Ȗ�2�C�&~��M�)X��Щ,��]��_���dw5!�.��Uk�B�_��w�WM�3�i�Z����7���qG�'>У�s�4�G� �������#E3]C�����&���n���B���%³�8�iV��}K^�5��ć��
��b��C�"DňV+�ˍr�sE�#����q1�Op��	b��7�<|9�M�+݋2�R�.�@��(%�ֱ��/��(*̑���j�� 0ς	9LF��	J�|�}Fĩ����LF�9�3���<�!;F�}��Ef�mIT��ׯ�0�2nR��|O �B)��(L�2z�_�6��0y���l��@�1t�h;����J×��蚦�\�Ez����]�ec�G�7�j~Bw!#/��kJ^qT�.��ʅU�{������ ���b�w|��m��� ई�'����фʀK�a���I-�����L�.so|߬��p֑������	C��_6�CV����xa��h����L��MH/�_�y�j�Z�;^忮�Ą̽�MVm%��/�e����� Ip������������`�� �7�MěȔ�zr:*�ʼ��>K�=b��F�5�=|�y���}����LC�)"O�XCMI�1u,�Mx�YJF�@��gݵ��'O�g��?��x�+6����'��<!3�Y�ߘJ�s,�T7�<z�Ш�
R;�o�B��:#�g�\i��gDf<I��8H`Hm	I��;d9<������]����qNS��O�� �r �������
рC� !C����Z�2�A�L�5rO�8I�գ�l��*XҾ�k��_��Մ��2�_���(���_!����߫�����f��Z��q�򿛏��N���������xͲﺡ�e5A��o �/Q4q I�����RmG[Jt�(�]�4�̮�sIюy~{���kݐ̗��E��_8�C���D�mxC�ڙ3�e���Q���HJe{�gL`�6�����0�Ik�5�a�jy��eml�R<����eR{E������S�{լOV7^���s��}�7B<�	m�wy�d.TB1/Y- .�O�Y�L�/�j�x,�pQE~"4p�+sz�|��?o>��㼆g`����Rc䞘vإ���\8��Qt$B�%EQ(}�1����,*�XcW�HUQ�U��a��v\���I��jRi:*'ί���2o0�`bה^�ae�rGHb#{4R&9��cJVm*8A����S|�	�8�p�Ϡ6x����j=|���sN?Q�?O�S���Nn���b����������ʩ6�_k�3��E����W������g����+�;�1d�.�ۇڒ*�u���N�9뉋�\��r~zѤs����U9�A��.~�籅R��(�B:���a��s����� �]�5yx�b�;��j�ς�g��m�Xr'�8���Hyfl;�r����k�\ԉ����?�g�?z�/wE�'���u`�����Ŗ _����X�����_?�_9������_l.����{8�(�N��|�~p��8���|�1��@�ŰF(�g=�"=s�@3sv��yRԫ���`Hv(�Н�����B�I<���-��WxO�~�M����,�8p�A��ѷ�ӥ�]g2^�k�*�W������z�Q6�ں50'���`q<g�u����|o�Q͹�ET�vG\��H0r7���]��p@#_�;�����c۝�:�e��H���F5
���cJ�,�*22ZZI�I���$�Ov�X`��Г-a�p�#�ԉ��1� �+>'*#EuZ$�V�i�x�V���<N!��S`�Bod3��lm3�c!`�0�3�c���)�m��������c38b:祮>P��D�/%HکQ6~���������G�ŨF��"�����AEͬ4\u2JŚ���p3�d�����y��ł�/�����Y���P�l��Qo������B�i�?'���1ikܙzp0���D���4����B��"�yx�;",2�;���%��yil��ʶ��$)���Q������=�Y�狮%���/���ji��4I��ED����Ŧ/���U�P����+.����A��$f`�d ���o�
��_���O�WN������U��Q���; ����@�)\�	@�#��X9�(O9r}:/{<'���$�q#��T�u��P\��G�HJ���-��ON��3�x�ŝ��mt�����JKc�)UbzZ�;a� ������MۭSj�/N I~f�֝��$Jw�s�a2;6G+X�F�
I����6��{�cy��q��<ޖ�������n�q{|�o�~v���6�Rqɴ��[+�aѵ�z)�V&f�ՖӔ���c�k^�����,H�������T���~�z��Y����3�sQ����&�qN��I�>��|�����/���_��7@�WN���-ի��������g���g`��^��@}@%v ���!�P�G��{��S�o d�v�I�و�l8= ��l�&41����җ����^�������u�j2��?x�M�ø�T�V�z���j�������'kE��� $�@t@��f�à|��'��#�������NP��G�v�!0Ʒ�?G;�=b*0R�Sj%�Q�3u*�Z������g��?���a"jX�[DKG��`��̥�e�~%�/��J�?�v��'��t<���&�w	�c��J�.����.� NԗF����A_9���D�>��S.�N��[j31����3��|��6׃�����h���{4!���lϳ��r٣r薌���-�d�s8�y����:۟��	}-����gv�Ҁ	� �!�B��5sԛ����v�1,��>m`Ep���!N������IGaT������>+���$F�w��Wz����Ç���w;k;ۻ�~���`����~b�����:߉&�.��]��~pZ�_"���i�{���"8�D*Na�[�/5��[�!~ڛ�|a�#�Q-�}��QiuE�g��0'0�.sC3��R�B`y�Bg�q��=;�����ް�f���Es��i��Xf/����,�N�OYP��5��aq�u����{q1�%�WJr S�S�zQקȥ�@.딌߁�ǱB~��J�@}A��F���P�
������S���_��R&�K���x�t�Wn��l(��s�����r��}�$3H8T���?e�L`/њn���'�jpR]i,>ȏ��[�	HF_�ɔ�1ߝ@n8b�Z�)�擟�)�|筦%�y�ƪ��#g'
�� ���wTVK���EM���G���ʢ��D���9&�Rw�m̋���������}D�>";���nN1�ܙ��IK��6R�����_�=���5�Я=Ae	8��P	�i�.�����������G���\X�J���a���_����ԁ����e��.֊���W������������WkK?�ł��}����v��e������K�Z����_;�_=��������
����?O�;a��?_������zw�����1���8���oIh�)'���j��8U����Qê:�'/�+z)[;�	D^�v�<�I(��*Rʎ�l�r�,*�]�]X�<r��$�:��#��/}����-uE)�	��SOЪ�kh�����ſ	&F��XU(���D/�:ݕH�Z|��|�+&������Zf��:^{�a9=�����:3�b���=��~��qxK���}��_���LH&�Av�WyJK�T�/uk{8q�FOͣx9�7���&=�45;��W
)B!�<��&�|Uk9�����#��>X�k��T?��@g���Ji�!��ہb]Y��c�ʼ��MEi�Y�1T� <c����w���8vDP�8�s��4Zz�@��Zw��;8(b6kZ^{��ɥ!ጩ�K��J��R��hIܞ�;�w��C�|)��x��]u����`%K�¢]��_��{?�Y�7���w��*�?�_�����_=�Ͼ��6i�_l��7p�G#3���O�3HC|ӿ���Lr�5=y}�e���#�D���ܜ�Œ7g�:�"�m[O\.�m*\5�o�mO�;���H�D�I����a�e�u�OUW��}}^4�Z��}օ�o��ԕV��u�MV$['.����[���J�L��ş���f&#�r�o=�3����g��+.��M�Y��1B���[�����_m����_��];�_=�Ϧ������o���}#�_`�-JRx�S�#�u*��l't�Q�bM��ÍM��J55'����3~��~��;2�@�B�A���xӬ���T8������y�4��]'Мe�x�Y�s�snP���h?����8�I'K+a'E2KQfݣfB�6?�jSqO�oC�H�e���Vsl��ZKUVk4k�Z�Ϊ�&p �_�sR$~�]:ơM%Z���!�)�5b�ȓ��랴��iѰ����Ҁ*�̦����-���i5��Ih��4aͲ���c�h����� D�F��y��\?�˚͆�n,5�~5�s�����}oM����V�/�m6�MiT������[�j�x�׶67�k���C�/_y��aX����6�CIUxM�v�'�8��ewڿ�m��9 t,jD�E�1(�y�>cB����,|�����oZ��ssU�o����������x�eY�^V_�����yz��RN�����nŮZs�����)O�H���lG� �/)ѐ�!��6�d�o�~�Y"w��<.܀f���w�d@�+�%X.��DR���[�c�t����jN��E�C1Fx���쬱�/�X]_߹�
ښ�G�O�D o�$��Y.����:��c��z�����7y3Q�@��d�����v�Q�uLP��r�e[�Y�B��ʺ0�0��f�����D���m6�<�h����ȱ阇���35!t�Q�i�a�N$M��9 �gN�x#ȶrhڱm�>�pb�}��E��_B���L���d�ޕ&�O7ͷ�O� ]�j���	d���y:t	<+׫W��U�۳�@o3򬙽�e�!,��Ѭ���t-��4#�k��_0���u1 Ќܿø��D����-�p<�s����o�$��b���jk�9�������D�q42�%0Hُ������d��%����'����P�=/�/�v�`k�k��J�zɡ�������{�X���O�]���.�����@7%��:{�譋~r`=�c14}d���ȟ��a��#������C�o���ɍ��&�kCn�1�9��8�kX��r�C(���H`���<�_�������vw�+��c�E�s:�2@LB:���sYX��;���-�󣡎�x�e���K�2�C���V�����D��Q�*U�vE���G�s���.'��Txz���v{~�+N�4>9���մ�����C�A,m���,)�W��������5�Ur�T��߫HN�R��7���t)Q��3���Uz����O���3*ʖ�۱�%Ƨmظa肹�����%Q@�.�Z����2K>�B�F��I'�Jw��(���Zj"-sgW�i��UW8�j&���v�(��?KC�����,�����o#��m���?�8�h�U/��������:�T�9���7[K����&�v�`���G�Yp�����*��b�/����������ji�o֋��kyp���d��<nl~0�jg�z�^������N���;�}���y��I��l�����ڻ�����?��o�mm�z����q�9��'s}{���v���As�ޯ�O�����N�z\?�=�:�~��5_T7�??Y�Y��w����j����ss�+����<;�<~��{?�i�λ�����u���ps4|���nW��~���<?s?��&��kG��'���������?^��mz�;�iuPmN�����w��i��?��~��p����i���������V������k���������^���r�x����ON����҇_��'�w�������{>��{������ɓ�M�q��[0���������^������r�B��i�	������%���-�5��5[����_;�_=�Ϧ��b6�Wa�u=��W�"�*E����C��쮽�괻�v6v��`%����Ep0�.V�wr�:����A�]�����_Џ����<�����z���b�/�������g��UK��b���o~��Mr����^�_/�~o�ww���p�x�A�'M��7a�{����������?u3�>�/��+�������bq�[��������S����F����B�w-OϷP�+6���p��� ���l��F:D��%66O��5fh��	 'C;���;���{pi�Q$�R�7��Z�\k.�?��z�\�5��zNz�~��j����:�|�Z�7��z5@�V�5�������z�A�ek�k���Qa��������*��b�/���������K��?�	�-���R�]�G�I}v^�;��ukO��<����:+n7��52ϔw����$`A`;��EG5}�/��0���$���b���B!t�o���0;([��ad=ц*Z�c�R�Qٳ�@¡(�F�e�>�gsd4&���Q�t0qh)A�L���9�bɾ�%#F�b�L�i���,��Oٍ���&';��o�g��;/%`�Ȳ<�i9O�g��?��(a	��!���5��b�/��������g�c��������<��A�՛�vx��?�0�M�젥"nAV@�d�A���]����m�=����!���C��=6G�f}5�|kY)�3��,l�Zи7�xU�J�������7V�b�N�Q�g�}vw�q�9�������̷D�`+\lI��u[�k!.=XpL��~g���94�9���P$���v�Ν�;�o�2a~��!���R����[���C ���܁<�T�g�
���/O��w҄������j�����gn�}�\h�ʣE�e[��; ��7l�f��&	��{�K��e����B�rF|$��9����|��p=�o��@X���߹�z�DdZ��c2�ͽ��L6�kgZ+bn[����d�9Mf�'�_��+>�\	�67�O������.'{����=�vq9��"?q85��-�eq��5�x�$�H���Q?1�!/E�`R|�,�r������5�sE�."��3����x��<�e��l�@��6�u�� ���v@�A��/�^]`�o30Ĳ�$4G4c$��
�^_�-�ڭ��ŉ����䬚�f��
���a�ԧ�;�v�?A8!"Ie��9~�w`�`�=K��"y��]�����mr:oNs�7&�45
�F�Tj2b|� �0��������EB��P�v���b��ރo��W����w���?����V�����ky|��	�Ng�m�l����f��P��џ��z~rA@�0�W�+��U���j���b�/�����*�M���V���Ղ����Wq�U:J]{;�^@��'�uD����Y��N�0�a���}l%/��\�~.���w� ��A������, ����_7�_=�_���Q������k��c^A�e2�Z�x��B�P���JQ/�Ҧ�[�Y�0KXA�7���{/ݴ�W������_;�_=��������_K���kyn������>Y�_t��Ȏ�g��cLNɹ5:�?��?�\�oh��_`5|�z+3ֱ,Q���y�= �l�[��
S��зB�e�_� �����X��x����Ѩ,���W��jb��FU�^Ď@�n�	�ȐP��(��g1ߝ�;�|{ �ĥ�B���({Z�lºKj���Up�$`�]��˫N�u_:D�0$��pu09<D���҉P�:��������@=Z��� ���rF#����	���:j�$�P�̣F&#�R'3�.�F8�Hy�x�BV���!���g�3�R|��76OǤ�A�U�5�q"u	p������Ф&GBY@FR�Q!���!���1���,�`8��j����9c-[(#�*��z<�o�4,����L~˧�Nu-�(�nE�/�y������Q_xb׷B߶�D��ey戂���߯V�e���Z[������&���؟9�< �����TS}�����V�~�ՖZ�V���w��O����w��X6�+�ҥx�'��xH�d��Z�ъ0�s���!tg!��'a/�|�$�E�'9�0�ۻ��_�;�zÔ�U���R�t��.6����=A�hK��R�bR\*�g�P��p�r�u�����k���/-����:��۷Np'IFW�˗mycs{o������w�_���m���Ϋ՝u����-�m��f3R�0\J��l��Xe�1��$����શ�y�^_�i�X�m�3㝀����{�I6��	�6ۯ�#����}K�� �M{h��h�A��j<YC �\⤰����{�28M�_�*H���Z�a�b`��N�W�fg��1�����104:u�6Ƈ��O������Q��y���:����/7v�]���)�������W!�+�7u�w������4�7j���z���&�v�c���<��}M��oS���B5�D�O�O�O�O�O�O�O�O�O�O�O�O�O�O�O�O�O�O�O�O�|'��oGU_ � 